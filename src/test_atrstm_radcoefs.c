/* Copyright (C) 2022, 2023 |Méso|Star> (contact@meso-star.com)
 * Copyright (C) 2020, 2021 Centre National de la Recherche Scientifique
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "atrstm_radcoefs.h"

int
main(int argc, char** argv)
{
  const double ka_ref = 5.7382401729092799E-1;
  const double ks_ref = 7.2169062018378995E-6;

  struct radcoefs radcoefs = RADCOEFS_NULL;
  struct radcoefs_compute_args args = RADCOEFS_COMPUTE_ARGS_NULL;
  (void)argc, (void)argv;

  args.lambda = 633;
  args.n = 1.90;
  args.kappa = 0.55;
  args.fractal_prefactor = 1.70;
  args.fractal_dimension = 1.75;
  args.soot_volumic_fraction = 1.e-7;
  args.soot_primary_particles_count = 100;
  args.soot_primary_particles_diameter = 1;
  args.radcoefs_mask = ATRSTM_RADCOEFS_MASK_ALL;

  radcoefs_compute(&radcoefs, &args);

  printf("ka = %g; ks = %g\n", radcoefs.ka, radcoefs.ks);
  CHK(eq_eps(radcoefs.ka, ka_ref, ka_ref*1.e-6));
  CHK(eq_eps(radcoefs.ks, ks_ref, ks_ref*1.e-6));
  CHK(eq_eps(radcoefs.kext, ka_ref+ks_ref, (ka_ref+ks_ref)*1e-6));

  args.radcoefs_mask = ATRSTM_RADCOEF_FLAG_Ka;
  radcoefs_compute(&radcoefs, &args);
  CHK(eq_eps(radcoefs.ka, ka_ref, ka_ref*1.e-6));
  CHK(radcoefs.ks == 0);
  CHK(radcoefs.kext == 0);

  args.radcoefs_mask = ATRSTM_RADCOEF_FLAG_Ks;
  radcoefs_compute(&radcoefs, &args);
  CHK(radcoefs.ka == 0);
  CHK(eq_eps(radcoefs.ks, ks_ref, ks_ref*1.e-6));
  CHK(radcoefs.kext == 0);

  args.radcoefs_mask = ATRSTM_RADCOEF_FLAG_Kext;
  /* Note that actually even though Ka and Ks are note required they are
   * internally computed and thus are returned to the caller */
  radcoefs_compute(&radcoefs, &args);
  CHK(eq_eps(radcoefs.kext, ka_ref+ks_ref, (ka_ref+ks_ref)*1e-6));

  args.radcoefs_mask = ATRSTM_RADCOEFS_MASK_ALL;

  args.soot_primary_particles_count = 0;
  radcoefs_compute(&radcoefs, &args);
  CHK(radcoefs.ka == 0);
  CHK(radcoefs.ks == 0);
  CHK(radcoefs.kext == 0);

  args.soot_primary_particles_count = 100;
  args.soot_volumic_fraction = 0;
  radcoefs_compute(&radcoefs, &args);
  CHK(radcoefs.ka == 0);
  CHK(radcoefs.ks == 0);
  CHK(radcoefs.kext == 0);

  return 0;
}

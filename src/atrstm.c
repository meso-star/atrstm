/* Copyright (C) 2022, 2023 |Méso|Star> (contact@meso-star.com)
 * Copyright (C) 2020, 2021 Centre National de la Recherche Scientifique
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#define _POSIX_C_SOURCE 200112L /* snprintf & lround support */

#include "atrstm.h"
#include "atrstm_c.h"
#include "atrstm_cache.h"
#include "atrstm_log.h"
#include "atrstm_setup_octrees.h"

#include <astoria/atrtp.h>

#include <star/suvm.h>
#include <star/svx.h>

#include <rsys/cstr.h>
#include <rsys/double3.h>

#include <omp.h>

/*******************************************************************************
 * Helper functions
 ******************************************************************************/
static int
check_args(const struct atrstm_args* args)
{
  if(!args
  || !args->sth_filename
  || (args->spectral_type == ATRSTM_SPECTRAL_LW && !args->atrck_filename)
  || !args->atrtp_filename
  || !args->atrri_filename
  || !args->name
  || (unsigned)args->spectral_type >= ATRSTM_SPECTRAL_TYPES_COUNT__
  || args->wlen_range[0] > args->wlen_range[1]
  || args->fractal_prefactor <= 0
  || args->fractal_dimension <= 0
  || args->optical_thickness < 0
  || !args->nthreads) {
    return 0;
  }

  if(args->auto_grid_definition) {
    if(!args->auto_grid_definition_hint)
      return 0;
  } else {
    if(!args->grid_max_definition[0]
    || !args->grid_max_definition[1]
    || !args->grid_max_definition[2])
      return 0;
  }

  return 1;
}

static FINLINE unsigned
round_pow2(const unsigned val)
{
  const unsigned next_pow2 = (unsigned)round_up_pow2(val);
  if(next_pow2 - val <= next_pow2/4) {
    return next_pow2;
  } else {
    return next_pow2/2;
  }
}

static void
compute_default_grid_definition
  (const struct atrstm* atrstm,
   const unsigned definition_hint,
   unsigned def[3])
{
  double low[3];
  double upp[3];
  double sz[3];
  double sz_max;
  double vxsz;
  int iaxis_max;
  int iaxis_remain[2];
  int i;
  ASSERT(atrstm && def);

  SUVM(volume_get_aabb(atrstm->volume, low, upp));
  sz[0] = upp[0] - low[0];
  sz[1] = upp[1] - low[1];
  sz[2] = upp[2] - low[2];

  /* Define the dimension along which the volume AABB is the greatest */
  sz_max = -DBL_MAX;
  FOR_EACH(i, 0, 3) {
    if(sz[i] > sz_max) { iaxis_max = i; sz_max = sz[i]; }
  }

  /* Define the other axes */
  iaxis_remain[0] = (iaxis_max + 1) % 3;
  iaxis_remain[1] = (iaxis_max + 2) % 3;

  /* Fix the definition to along the maximum axis and compute the voxel size
   * along this axis */
  def[iaxis_max] = round_pow2(definition_hint);
  vxsz = sz[iaxis_max] / def[iaxis_max];

  /* Compute the definition along the 2 remaining axes. First, compute them by
   * assuming that the voxels are cubics and then round these definitions to
   * their nearest power of two. */
  def[iaxis_remain[0]] = (unsigned)lround(sz[iaxis_remain[0]]/vxsz);
  def[iaxis_remain[1]] = (unsigned)lround(sz[iaxis_remain[1]]/vxsz);
  def[iaxis_remain[0]] = round_pow2(def[iaxis_remain[0]]);
  def[iaxis_remain[1]] = round_pow2(def[iaxis_remain[1]]);
}

static void
release_atrstm(ref_T* ref)
{
  struct atrstm* atrstm;
  ASSERT(ref);
  atrstm = CONTAINER_OF(ref, struct atrstm, ref);
  octrees_clean(atrstm);
  if(atrstm->atrtp) ATRTP(ref_put(atrstm->atrtp));
  if(atrstm->atrri) ATRRI(ref_put(atrstm->atrri));
  if(atrstm->suvm) SUVM(device_ref_put(atrstm->suvm));
  if(atrstm->volume) SUVM(volume_ref_put(atrstm->volume));
  if(atrstm->svx) SVX(device_ref_put(atrstm->svx));
  if(atrstm->cache) cache_ref_put(atrstm->cache);
  if(atrstm->logger == &atrstm->logger__) logger_release(&atrstm->logger__);
  str_release(&atrstm->name);
  ASSERT(MEM_ALLOCATED_SIZE(&atrstm->svx_allocator) == 0);
  mem_shutdown_proxy_allocator(&atrstm->svx_allocator);
  MEM_RM(atrstm->allocator, atrstm);
}

/*******************************************************************************
 * Exported functions
 ******************************************************************************/
res_T
atrstm_create
  (struct logger* logger, /* NULL <=> use default logger */
   struct mem_allocator* mem_allocator, /* NULL <=> use default allocator */
   const struct atrstm_args* args,
   struct atrstm** out_atrstm)
{
  struct atrstm* atrstm = NULL;
  struct mem_allocator* allocator = NULL;
  int nthreads_max;
  res_T res = RES_OK;

  if(!out_atrstm || !check_args(args)) {
    res = RES_BAD_ARG;
    goto error;
  }

  allocator = mem_allocator ? mem_allocator : &mem_default_allocator;
  atrstm = MEM_CALLOC(allocator, 1, sizeof(*atrstm));
  if(!atrstm) {
    if(args->verbose) {
      #define ERR_STR "Could not allocate the AtrSTM data structure.\n"
      if(logger) {
        logger_print(logger, LOG_ERROR, ERR_STR);
      } else {
        fprintf(stderr, MSG_ERROR_PREFIX ERR_STR);
      }
      #undef ERR_STR
    }
    res = RES_MEM_ERR;
    goto error;
  }
  nthreads_max = MMAX(omp_get_max_threads(), omp_get_num_procs());
  ref_init(&atrstm->ref);
  atrstm->allocator = allocator;
  atrstm->verbose = args->verbose;
  atrstm->nthreads = MMIN(args->nthreads, (unsigned)nthreads_max);
  atrstm->fractal_prefactor = args->fractal_prefactor;
  atrstm->fractal_dimension = args->fractal_dimension;
  atrstm->spectral_type = args->spectral_type;
  atrstm->wlen_range[0] = args->wlen_range[0];
  atrstm->wlen_range[1] = args->wlen_range[1];
  atrstm->optical_thickness = args->optical_thickness;

  str_init(allocator, &atrstm->name);

  if(logger) {
    atrstm->logger = logger;
  } else {
    setup_log_default(atrstm);
  }

  if(args->use_simd) {
    #ifdef ATRSTM_USE_SIMD
    atrstm->use_simd = 1;
    #else
    log_warn(atrstm,
      "AtrSTM cannot use the SIMD instruction set: "
      "it was compiled without SIMD support.\n");
    atrstm->use_simd = 0;
    #endif
  }

  if(args->spectral_type == ATRSTM_SPECTRAL_SW
  && args->wlen_range[0] != args->wlen_range[1]) {
    log_err(atrstm,
      "Invalid spectral range [%g, %g], only monochromatic computations are "
      "supported in shortwave.\n",
      args->wlen_range[0],
      args->wlen_range[1]);
    res = RES_BAD_ARG;
    goto error;
  }

  res = str_set(&atrstm->name, args->name);
  if(res != RES_OK) {
    log_err(atrstm, "Cannot setup the gas mixture name to `%s' -- %s.\n",
      args->name, res_to_cstr(res));
    goto error;
  }

  /* Setup the allocator used by the SVX library */
  res = mem_init_proxy_allocator(&atrstm->svx_allocator, atrstm->allocator);
  if(res != RES_OK) {
    log_err(atrstm,
      "Cannot initialise the allocator used to manager the Star-VoXel memory "
      "-- %s.\n", res_to_cstr(res));
    goto error;
  }

  /* Load the refractive index */
  res = atrri_create
    (atrstm->logger, atrstm->allocator, atrstm->verbose, &atrstm->atrri);
  if(res != RES_OK) goto error;
  res = atrri_load(atrstm->atrri, args->atrri_filename);
  if(res != RES_OK) goto error;

  /* In shortwave, pre-fetched the refractive index */
  if(atrstm->spectral_type != ATRSTM_SPECTRAL_SW) {
    atrstm->refract_id = ATRRI_REFRACTIVE_INDEX_NULL;
  } else {
    const double wlen = atrstm->wlen_range[0];
    ASSERT(atrstm->wlen_range[0] == atrstm->wlen_range[1]);
    res = atrri_fetch_refractive_index(atrstm->atrri, wlen, &atrstm->refract_id);
    if(res != RES_OK) {
      log_err(atrstm,
        "Could not fetch the refractive index of the shortwave wavelength %g "
        "-- %s.\n", wlen, res_to_cstr(res));
      goto error;
    }
  }

  /* Create the Star-UnstructuredVolumetricMesh device */
  res = suvm_device_create
    (atrstm->logger, atrstm->allocator, atrstm->verbose, &atrstm->suvm);
  if(res != RES_OK) goto error;

  /* Structure the volumetric mesh */
  res = setup_unstructured_volumetric_mesh
    (atrstm, args->precompute_normals, args->sth_filename, &atrstm->volume);
  if(res != RES_OK) goto error;

  /* Define the grid definition */
  if(args->auto_grid_definition) {
    compute_default_grid_definition
      (atrstm, args->auto_grid_definition_hint, atrstm->grid_max_definition);
  } else {
    atrstm->grid_max_definition[0] = args->grid_max_definition[0];
    atrstm->grid_max_definition[1] = args->grid_max_definition[1];
    atrstm->grid_max_definition[2] = args->grid_max_definition[2];
  }

  /* Load the thermodynamic properties of the volumetric mesh */
  res = atrtp_create
    (atrstm->logger, atrstm->allocator, atrstm->verbose, &atrstm->atrtp);
  if(res != RES_OK) goto error;
  res = atrtp_load(atrstm->atrtp, args->atrtp_filename);
  if(res != RES_OK) goto error;

  /* Create the Star-VoXel device */
  res = svx_device_create
    (atrstm->logger, &atrstm->svx_allocator, atrstm->verbose, &atrstm->svx);
  if(res != RES_OK) goto error;

  /* Create the cache if necessary */
  if(args->cache_filename) {
    res = cache_create(atrstm, args->cache_filename, &atrstm->cache);
    if(res != RES_OK) goto error;
  }

  /* Build the octree */
  res = setup_octrees(atrstm);
  if(res != RES_OK) goto error;

exit:
  if(out_atrstm) *out_atrstm = atrstm;
  return res;
error:
  if(atrstm) { ATRSTM(ref_put(atrstm)); atrstm = NULL; }
  goto exit;
}

res_T
atrstm_ref_get(struct atrstm* atrstm)
{
  if(!atrstm) return RES_BAD_ARG;
  ref_get(&atrstm->ref);
  return RES_OK;
}

res_T
atrstm_ref_put(struct atrstm* atrstm)
{
  if(!atrstm) return RES_BAD_ARG;
  ref_put(&atrstm->ref, release_atrstm);
  return RES_OK;
}

const char*
atrstm_get_name(const struct atrstm* atrstm)
{
  ASSERT(atrstm);
  return str_cget(&atrstm->name);
}

void
atrstm_get_aabb(const struct atrstm* atrstm, double lower[3], double upper[3])
{
  struct svx_tree_desc tree_desc = SVX_TREE_DESC_NULL;
  ASSERT(atrstm && lower && upper);
  SVX(tree_get_desc(atrstm->octree, &tree_desc));
  d3_set(lower, tree_desc.lower);
  d3_set(upper, tree_desc.upper);
}

res_T
atrstm_at
  (const struct atrstm* atrstm,
   const double pos[3],
   struct suvm_primitive* prim, /* Volumetric primitive */
   double bcoords[4]) /* `pos' in `prim' */
{
  res_T res = RES_OK;

  if(!atrstm || !pos || !prim || !bcoords) {
    res = RES_BAD_ARG;
    goto error;
  }

  /* Find the primitive into which pos lies */
  res = suvm_volume_at(atrstm->volume, pos, prim, bcoords);
  if(res != RES_OK) {
    log_err(atrstm,
      "Error accessing the volumetric mesh at {%g, %g, %g} -- %s.\n",
      SPLIT3(pos), res_to_cstr(res));
    goto error;
  }

exit:
  return res;
error:
  goto exit;
}

/*******************************************************************************
 * Local functions
 ******************************************************************************/
void
dump_memory_size
  (const size_t size, /* In Bytes */
   size_t* real_dump_len, /* May be NULL */
   char* dump, /* May be NULL */
   size_t max_dump_len)
{
  size_t available_dump_space = max_dump_len;
  char* dst = dump;
  const size_t KILO_BYTE = 1024;
  const size_t MEGA_BYTE = 1024*KILO_BYTE;
  const size_t GIGA_BYTE = 1024*MEGA_BYTE;
  size_t ngigas, nmegas, nkilos;
  size_t nbytes = size;

  #define DUMP(Size, Suffix)                                                   \
    {                                                                          \
      const int len = snprintf                                                 \
        (dst, available_dump_space,                                            \
         "%li %s", (long)Size, Size > 1 ? Suffix "s ": Suffix " ");            \
      ASSERT(len >= 0);                                                        \
      if(real_dump_len) {                                                      \
        *real_dump_len += (size_t)len;                                         \
      }                                                                        \
      if((size_t)len < available_dump_space) {                                 \
        dst += len;                                                            \
        available_dump_space -= (size_t)len;                                   \
      } else if(dst) {                                                         \
        available_dump_space = 0;                                              \
        dst = NULL;                                                            \
      }                                                                        \
    } (void) 0

  if((ngigas = nbytes / GIGA_BYTE) != 0) {
    DUMP(ngigas, "GB");
    nbytes -= ngigas * GIGA_BYTE;
  }
  if((nmegas = nbytes / MEGA_BYTE) != 0) {
    DUMP(nmegas, "MB");
    nbytes -= nmegas * MEGA_BYTE;
  }
  if((nkilos = nbytes / KILO_BYTE) != 0) {
    DUMP(nkilos, "kB");
    nbytes -= nkilos * KILO_BYTE;
  }
  if(nbytes) {
    DUMP(nbytes, "Byte");
  }

  /* Remove last space */
  if(real_dump_len) *real_dump_len -= 1;
  if(dst != dump && dst[-1] == ' ') dst[-1] = '\0';

  #undef DUMP
}

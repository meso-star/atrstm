/* Copyright (C) 2022, 2023 |Méso|Star> (contact@meso-star.com)
 * Copyright (C) 2020, 2021 Centre National de la Recherche Scientifique
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef ATRSTM_H
#define ATRSTM_H

#include <star/suvm.h>
#include <star/svx.h>

#include <rsys/rsys.h>

#include <float.h>
#include <limits.h>

/* Library symbol management */
#if defined(ATRSTM_SHARED_BUILD) /* Build shared library */
  #define ATRSTM_API extern EXPORT_SYM
#elif defined(ATRSTM_STATIC) /* Use/build static library */
  #define ATRSTM_API extern LOCAL_SYM
#else /* Use shared library */
  #define ATRSTM_API extern IMPORT_SYM
#endif

/* Helper macro that asserts if the invocation of the atrstm function `Func'
 * returns an error. One should use this macro on suvm function calls for
 * which no explicit error checking is performed */
#ifndef NDEBUG
  #define ATRSTM(Func) ASSERT(atrstm_ ## Func == RES_OK)
#else
  #define ATRSTM(Func) atrstm_ ## Func
#endif

/* Radiative coefficients */
enum atrstm_radcoef {
  ATRSTM_RADCOEF_Ks, /* Scattering coefficient */
  ATRSTM_RADCOEF_Ka, /* Absorption coefficient */
  ATRSTM_RADCOEF_Kext, /* Extinction coefficient = Ks + Ka */
  ATRSTM_RADCOEFS_COUNT__
};

enum atrstm_radcoef_flag {
  ATRSTM_RADCOEF_FLAG_Ks = BIT(ATRSTM_RADCOEF_Ks),
  ATRSTM_RADCOEF_FLAG_Ka = BIT(ATRSTM_RADCOEF_Ka),
  ATRSTM_RADCOEF_FLAG_Kext = BIT(ATRSTM_RADCOEF_Kext),
  ATRSTM_RADCOEFS_MASK_ALL =
    ATRSTM_RADCOEF_FLAG_Ks
  | ATRSTM_RADCOEF_FLAG_Ka
  | ATRSTM_RADCOEF_FLAG_Kext
};

/* List of medium components */
enum atrstm_component {
  ATRSTM_CPNT_SOOT,
  ATRSTM_CPNT_GAS,
  ATRSTM_CPNTS_COUNT__
};

enum atrstm_component_flag {
  ATRSTM_CPNT_FLAG_SOOT = BIT(ATRSTM_CPNT_SOOT),
  ATRSTM_CPNT_FLAG_GAS = BIT(ATRSTM_CPNT_GAS),
  ATRSTM_CPNTS_MASK_ALL = ATRSTM_CPNT_FLAG_SOOT | ATRSTM_CPNT_FLAG_GAS
};

enum atrstm_svx_op {
  ATRSTM_SVX_OP_MIN,
  ATRSTM_SVX_OP_MAX,
  ATRSTM_SVX_OPS_COUNT__
};

enum atrstm_svx_op_flag {
  ATRSTM_SVX_OP_FLAG_MIN = BIT(ATRSTM_SVX_OP_MIN),
  ATRSTM_SVX_OP_FLAG_MAX = BIT(ATRSTM_SVX_OP_MAX),
  ATRSTM_SVX_OPS_MASK_ALL = ATRSTM_SVX_OP_FLAG_MIN | ATRSTM_SVX_OP_FLAG_MAX
};

enum atrstm_spectral_type {
  ATRSTM_SPECTRAL_LW, /* Longwave */
  ATRSTM_SPECTRAL_SW, /* Shortwave */
  ATRSTM_SPECTRAL_TYPES_COUNT__
};

struct atrstm_args {
  const char* sth_filename; /* Filename of the Star-TetraHedra mesh */
  const char* atrck_filename; /* Filename of the Correlated K */
  const char* atrtp_filename; /* Filename of the Thermodynamic properties */
  const char* atrri_filename; /* Filename of the refraction index LUT */
  const char* cache_filename; /* Cache to use. NULL <=> no cache */
  const char* name; /* Name of the medium */

  enum atrstm_spectral_type spectral_type; /* Longwave/shortwave */
  double wlen_range[2]; /* Spectral range to handle In nm */

  double fractal_prefactor;
  double fractal_dimension;

  unsigned grid_max_definition[3]; /* Fixed grid definition along the 3 axes */
  unsigned auto_grid_definition_hint; /* Hint on the grid definition to eval */
  int auto_grid_definition; /* Switch between auto and fixed grid definition */

  double optical_thickness; /* Threshold used during octree building */

  int precompute_normals; /* Pre-compute the tetrahedra normals */
  int use_simd; /* Enable the use of the SIMD instruction set if available */

  unsigned nthreads; /* Hint on the number of threads to use */
  int verbose; /* Verbosity level */
};

#define ATRSTM_ARGS_DEFAULT__ {                                                \
  NULL, /* STh filename */                                                     \
  NULL, /* AtrCK filename */                                                   \
  NULL, /* AtrTP filename */                                                   \
  NULL, /* AtrRI filename */                                                   \
  NULL, /* Cache filename */                                                   \
  "semi transparent medium", /* Name */                                        \
                                                                               \
  ATRSTM_SPECTRAL_SW, /* Spectral type */                                      \
  {500,500}, /* Spectral integration range */                                  \
                                                                               \
  1.70, /* Fractal prefactor */                                                \
  1.75, /* Fractal dimension */                                                \
                                                                               \
  {256, 256, 256}, /* Acceleration grid max definition */                      \
  256, /* Hint on grid Definition in 'auto grid definition' mode */            \
  1, /* Enable/disable 'auto grid definition' mode */                          \
                                                                               \
  1, /* Optical thickness */                                                   \
                                                                               \
  0, /* Precompute tetrahedra normals */                                       \
  1, /* Use SIMD */                                                            \
                                                                               \
  (unsigned)~0, /* #threads */                                                 \
  0 /* Verbosity level */                                                      \
}
static const struct atrstm_args ATRSTM_ARGS_DEFAULT = ATRSTM_ARGS_DEFAULT__;

struct atrstm_fetch_radcoefs_args {
  struct suvm_primitive prim; /* Volumetric primitive to query */
  double bcoords[4]; /* Barycentric coordinates of the pos in prim to query */
  size_t iband; /* Spectral band index. Not use in shortwave */
  size_t iquad; /* Quadrature point index. Not use in shortwave */
  double wavelength; /* In nm */

  int radcoefs_mask; /* Combination of atrstm_radcoef_flag */
  int components_mask; /* Combination of atrstm_component_flag */

  /* For debug: assert if the fetched property is not in [k_min, k_max] */
  double k_min[ATRSTM_RADCOEFS_COUNT__];
  double k_max[ATRSTM_RADCOEFS_COUNT__];
};

#define ATRSTM_FETCH_RADCOEFS_ARGS_DEFAULT__ {                                 \
  SUVM_PRIMITIVE_NULL__,                                                       \
  {0, 0, 0, 1}, /* Barycentric coordinates */                                  \
  SIZE_MAX, /* Index of the spectral band */                                   \
  SIZE_MAX, /* Index of the quadrature point */                                \
  DBL_MAX, /* Wavelength */                                                    \
                                                                               \
  ATRSTM_RADCOEFS_MASK_ALL, /*  Mask of radiative properties to fetch */       \
  ATRSTM_CPNTS_MASK_ALL, /* Mask of component to handle */                     \
                                                                               \
  /* For debug */                                                              \
  {-DBL_MAX,-DBL_MAX,-DBL_MAX}, /* Kmin */                                     \
  { DBL_MAX, DBL_MAX, DBL_MAX}, /* Kmax */                                     \
}
static const struct atrstm_fetch_radcoefs_args
ATRSTM_FETCH_RADCOEFS_ARGS_DEFAULT = ATRSTM_FETCH_RADCOEFS_ARGS_DEFAULT__;

struct atrstm_fetch_radcoefs_svx_args {
  double pos[3]; /* Position to query */
  size_t iband; /* Spectral band index. Not use in shortwave */
  size_t iquad; /* Quadrature point index. Not use in shortwave */

  int radcoefs_mask; /* Combination of atrstm_radcoef_flag */
  int components_mask; /* Combination of atrstm_component_flag */
  int operations_mask; /* Combination of atrstm_svx_op_flag */
};

#define ATRSTM_FETCH_RADCOEFS_SVX_ARGS_DEFAULT__ {                             \
  {0, 0, 0}, /* Position */                                                    \
  SIZE_MAX, /* Index of the spectral band */                                   \
  SIZE_MAX, /* Index of the quadrature point */                                \
                                                                               \
  ATRSTM_RADCOEFS_MASK_ALL, /*  Mask of radiative properties to fetch */       \
  ATRSTM_CPNTS_MASK_ALL, /* Mask of component to handle */                     \
  ATRSTM_SVX_OPS_MASK_ALL, /* Mask of operations to query */                   \
}
static const struct atrstm_fetch_radcoefs_svx_args
ATRSTM_FETCH_RADCOEFS_SVX_ARGS_DEFAULT =
  ATRSTM_FETCH_RADCOEFS_SVX_ARGS_DEFAULT__;

struct atrstm_fetch_radcoefs_svx_voxel_args {
  struct svx_voxel voxel; /* Voxel to query */
  size_t iband; /* Spectral band index. Not use in shortwave */
  size_t iquad; /* Quadrature point index. Not use in shortwave */

  int radcoefs_mask; /* Combination of atrstm_radcoef_flag */
  int components_mask; /* Combination of atrstm_component_flag */
  int operations_mask; /* Combination of atrstm_svx_op_flag */
};

#define ATRSTM_FETCH_RADCOEFS_SVX_VOXEL_ARGS_DEFAULT__ {                       \
  SVX_VOXEL_NULL__, /* Voxel */                                                \
  SIZE_MAX, /* Index of the spectral band */                                   \
  SIZE_MAX, /* Index of the quadrature point */                                \
                                                                               \
  ATRSTM_RADCOEFS_MASK_ALL, /*  Mask of radiative properties to fetch */       \
  ATRSTM_CPNTS_MASK_ALL, /* Mask of component to handle */                     \
  ATRSTM_SVX_OPS_MASK_ALL, /* Mask of operations to query */                   \
}
static const struct atrstm_fetch_radcoefs_svx_voxel_args
ATRSTM_FETCH_RADCOEFS_SVX_VOXEL_ARGS_DEFAULT =
  ATRSTM_FETCH_RADCOEFS_SVX_VOXEL_ARGS_DEFAULT__;

struct atrstm_dump_svx_octree_args {
  size_t iband; /* Spectral band index. Not use in shortwave */
  size_t iquad; /* Quadrature point index. Not use in shortwave */
};

#define ATRSTM_DUMP_SVX_OCTREE_ARGS_DEFAULT__ {SIZE_MAX, SIZE_MAX}
static const struct atrstm_dump_svx_octree_args
ATRSTM_DUMP_SVX_OCTREE_ARGS_DEFAULT = ATRSTM_DUMP_SVX_OCTREE_ARGS_DEFAULT__;

struct atrstm_trace_ray_args {
  double ray_org[3];
  double ray_dir[3];
  double ray_range[2];

  svx_hit_challenge_T challenge; /* NULL <=> Traversed up to the leaves */
  svx_hit_filter_T filter; /* NULL <=> Stop RT at the 1st hit voxel */
  void* context; /* User data send to the 'challenge' & 'filter' function */

  size_t iband; /* Spectral band id. Not use in shortwave */
  size_t iquad; /* Quadrature point. Not use in short wave */
};

#define ATRSTM_TRACE_RAY_ARGS_DEFAULT__ {                                      \
  {0,0,0}, /* Ray origin */                                                    \
  {0,0,1}, /* Ray direction */                                                 \
  {0, DBL_MAX}, /* Ray range */                                                \
                                                                               \
  NULL, /* Challenge functor */                                                \
  NULL, /* Filter functor */                                                   \
  NULL, /* User defined data */                                                \
                                                                               \
  SIZE_MAX, /* Index of the spectral band */                                   \
  SIZE_MAX, /* Index of the quadrature point */                                \
}
static const struct atrstm_trace_ray_args ATRSTM_TRACE_RAY_ARGS_DEFAULT =
  ATRSTM_TRACE_RAY_ARGS_DEFAULT__;

struct atrstm_fetch_rdgfa_args {
  struct suvm_primitive prim; /* Volumetric primitive to query */
  double bcoords[4]; /* Barycentric coordinates of the pos in prim to query */
  double wavelength; /* In nm */
};

#define ATRSTM_FETCH_RDGFA_ARGS_DEFAULT__ {                                    \
  SUVM_PRIMITIVE_NULL__,                                                       \
  {0, 0, 0, 1}, /* Barycentric coordinates */                                  \
  DBL_MAX, /* Wavelength */                                                    \
}
static const struct atrstm_fetch_rdgfa_args
ATRSTM_FETCH_RDGFA_ARGS_DEFAULT = ATRSTM_FETCH_RDGFA_ARGS_DEFAULT__;

struct atrstm_rdgfa {
  double wavelength; /* In nm */
  double fractal_dimension;
  double gyration_radius;
};

#define ATRSTM_RDGFA_NULL__ {0,0,0}
static const struct atrstm_rdgfa ATRSTM_RDGFA_NULL =
  ATRSTM_RDGFA_NULL__;

/* Types used as syntactic sugar that store the radiative coefficients */
typedef double atrstm_radcoefs_T[ATRSTM_RADCOEFS_COUNT__];
typedef double atrstm_radcoefs_svx_T[ATRSTM_RADCOEFS_COUNT__][ATRSTM_SVX_OPS_COUNT__];

/* Forward declaration of extern data types */
struct logger;
struct mem_allocator;

/* Forward declaration of opaque data type */
struct atrstm;

BEGIN_DECLS

/*******************************************************************************
 * AtrSTM API
 ******************************************************************************/
ATRSTM_API res_T
atrstm_create
  (struct logger* logger, /* NULL <=> use default logger */
   struct mem_allocator* allocator, /* NULL <=> use default allocator */
   const struct atrstm_args* args,
   struct atrstm** atrstm);

ATRSTM_API res_T
atrstm_ref_get
  (struct atrstm* atrstm);

ATRSTM_API res_T
atrstm_ref_put
  (struct atrstm* atrstm);

ATRSTM_API const char*
atrstm_get_name
  (const struct atrstm* atrstm);

ATRSTM_API void
atrstm_get_aabb
  (const struct atrstm* atrstm,
   double lower[3],
   double upper[3]);

ATRSTM_API res_T
atrstm_at
  (const struct atrstm* atrstm,
   const double pos[3],
   struct suvm_primitive* prim, /* Volumetric primitive */
   double barycentric_coords[4]); /* `pos' in `prim' */

ATRSTM_API res_T
atrstm_fetch_radcoefs
  (const struct atrstm* atrstm,
   const struct atrstm_fetch_radcoefs_args* args,
   atrstm_radcoefs_T radcoefs); /* In m^-1 */

ATRSTM_API res_T
atrstm_fetch_radcoefs_svx
  (const struct atrstm* atrstm,
   const struct atrstm_fetch_radcoefs_svx_args* args,
   atrstm_radcoefs_svx_T radcoefs); /* In m^-1 */

ATRSTM_API res_T
atrstm_fetch_radcoefs_svx_voxel
  (const struct atrstm* atrstm,
   const struct atrstm_fetch_radcoefs_svx_voxel_args* args,
   atrstm_radcoefs_svx_T radcoefs); /* In m^-1 */

/* Trace a ray into the SVX octree data structure */
ATRSTM_API res_T
atrstm_trace_ray
  (const struct atrstm* atstm,
   const struct atrstm_trace_ray_args* args,
   struct svx_hit* hit);

ATRSTM_API res_T
atrstm_dump_svx_octree
  (const struct atrstm* atrstm,
   const struct atrstm_dump_svx_octree_args* args,
   FILE* stream);

ATRSTM_API res_T
atrstm_fetch_rdgfa
  (const struct atrstm* atrstm,
   const struct atrstm_fetch_rdgfa_args* args,
   struct atrstm_rdgfa* rdgfa);

END_DECLS

#endif /* ATRSTM_H */

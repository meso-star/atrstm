/* Copyright (C) 2022, 2023 |Méso|Star> (contact@meso-star.com)
 * Copyright (C) 2020, 2021 Centre National de la Recherche Scientifique
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef ATRSTM_SVX_H
#define ATRSTM_SVX_H

#include "atrstm.h"

/* SVX Memory layout
 * -----------------
 *
 * For each SVX voxel, the data of the optical property are stored
 * linearly as N single precision floating point data, with N computed as
 * below:
 *
 *  N = ATRSTM_RADCOEFS_COUNT__   #optical properties per voxel
 *    * ATRSTM_SVX_OPS_COUNT__ #supported operations on each properties
 *    * ATRSTM_CPNTS_COUNT__;  #components on which properties are defined
 *
 * In a given voxel, the index `id' in [0, N-1] corresponding to the optical
 * property `enum atrstm_radcoef P' of the component `enum atrstm_component
 * C' according to the operation `enum atrstm_svx_op O' is
 * then computed as bellow:
 *
 *  id = C * NFLOATS_PER_CPNT + P * ATRSTM_SVX_OPS_COUNT__ + O;
 *  NFLOATS_PER_CPNT = ATRSTM_SVX_OPS_COUNT__ * ATRSTM_RADCOEFS_COUNT__;
 */

/* Constant defining the number of floating point data per component */
#define NFLOATS_PER_CPNT (ATRSTM_SVX_OPS_COUNT__ * ATRSTM_RADCOEFS_COUNT__)

/* Constant defining the overall number of floating point data of a voxel */
#define NFLOATS_PER_VOXEL (NFLOATS_PER_CPNT * ATRSTM_CPNTS_COUNT__)

static FINLINE float
vox_get
  (const float* vox,
   const enum atrstm_component cpnt,
   const enum atrstm_radcoef prop,
   const enum atrstm_svx_op op)
{
  ASSERT(vox);
  ASSERT((unsigned)cpnt < ATRSTM_CPNTS_COUNT__);
  ASSERT((unsigned)prop < ATRSTM_RADCOEFS_COUNT__);
  ASSERT((unsigned)op < ATRSTM_SVX_OPS_COUNT__);
  return vox[cpnt*NFLOATS_PER_CPNT + prop*ATRSTM_SVX_OPS_COUNT__ + op];
}

static FINLINE void
vox_set
  (float* vox,
   const enum atrstm_component cpnt,
   const enum atrstm_radcoef prop,
   const enum atrstm_svx_op op,
   const float val)
{
  ASSERT(vox);
  ASSERT((unsigned)cpnt < ATRSTM_CPNTS_COUNT__);
  ASSERT((unsigned)prop < ATRSTM_RADCOEFS_COUNT__);
  ASSERT((unsigned)op < ATRSTM_SVX_OPS_COUNT__);
  vox[cpnt*NFLOATS_PER_CPNT + prop*ATRSTM_SVX_OPS_COUNT__ + op] = val;
}

static FINLINE void
vox_clear(float* vox)
{
  int cpnt;
  ASSERT(vox);

  FOR_EACH(cpnt, 0, ATRSTM_CPNTS_COUNT__) {
    int prop;
    FOR_EACH(prop, 0, ATRSTM_RADCOEFS_COUNT__) {
      vox_set(vox, cpnt, prop, ATRSTM_SVX_OP_MIN, FLT_MAX);
      vox_set(vox, cpnt, prop, ATRSTM_SVX_OP_MAX,-FLT_MAX);
    }
  }
}

#endif /* ATRSTM_SVX_H */


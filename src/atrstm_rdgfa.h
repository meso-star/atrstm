/* Copyright (C) 2022, 2023 |Méso|Star> (contact@meso-star.com)
 * Copyright (C) 2020, 2021 Centre National de la Recherche Scientifique
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef ATRSTM_RDGFA_H
#define ATRSTM_RDGFA_H

#include <rsys/rsys.h>
#include <math.h>

static FINLINE double /* In nanometer */
compute_gyration_radius
  (const double fractal_prefactor,
   const double fractal_dimension,
   const double soot_primary_particles_count,
   const double soot_primary_particles_diameter)
{
  const double kf = fractal_prefactor;
  const double Df = fractal_dimension;
  const double Np = soot_primary_particles_count;
  const double Dp = soot_primary_particles_diameter;
  const double Rg = 0.5 * Dp * pow(Np/kf, 1.0/Df); /* [nm] */
  return Rg;
}

#endif /* ATRSTM_RDGFA_H */


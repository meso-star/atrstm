/* Copyright (C) 2022, 2023 |Méso|Star> (contact@meso-star.com)
 * Copyright (C) 2020, 2021 Centre National de la Recherche Scientifique
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#define _POSIX_C_SOURCE 200112L /* nextafterf */

#include "atrstm_c.h"
#include "atrstm_cache.h"
#include "atrstm_log.h"
#include "atrstm_radcoefs.h"
#include "atrstm_partition.h"
#include "atrstm_setup_octrees.h"
#include "atrstm_svx.h"

#ifdef ATRSTM_USE_SIMD
  #include "atrstm_radcoefs_simd4.h"
#endif

#include <astoria/atrri.h>

#include <star/suvm.h>
#include <star/svx.h>

#include <rsys/clock_time.h>
#include <rsys/cstr.h>
#include <rsys/dynamic_array_size_t.h>
#include <rsys/morton.h>

#include <math.h>
#include <omp.h>

struct build_octree_context {
  struct atrstm* atrstm;
  struct pool* pool; /* Pool of voxel partitions */
  struct part* part; /* Current partition */

  /* Optical thickness threshold criteria for the merge process */
  double tau_threshold;
};
#define BUILD_OCTREE_CONTEXT_NULL__ { NULL, NULL, NULL, 0 }
static const struct build_octree_context BUILD_OCTREE_CONTEXT_NULL =
  BUILD_OCTREE_CONTEXT_NULL__;

/* Define the darray of list of primitive ids */
#define DARRAY_NAME prims_list
#define DARRAY_DATA struct darray_size_t
#define DARRAY_FUNCTOR_INIT darray_size_t_init
#define DARRAY_FUNCTOR_RELEASE darray_size_t_release
#define DARRAY_FUNCTOR_COPY darray_size_t_copy
#define DARRAY_FUNCTOR_COPY_AND_RELEASE darray_size_t_copy_and_release
#include <rsys/dynamic_array.h>

/*******************************************************************************
 * Helper functions
 ******************************************************************************/
static INLINE int
check_octrees_parameters(const struct atrstm* atrstm)
{
  return atrstm
      && atrstm->grid_max_definition[0]
      && atrstm->grid_max_definition[1]
      && atrstm->grid_max_definition[2]
      && atrstm->optical_thickness >= 0;
}

static INLINE void
register_primitive
  (const struct suvm_primitive* prim,
   const double low[3],
   const double upp[3],
   void* context)
{
  struct darray_size_t* prims = context;
  ASSERT(prim && low && upp && context);
  ASSERT(low[0] < upp[0]);
  ASSERT(low[1] < upp[1]);
  ASSERT(low[2] < upp[2]);
  (void)low, (void)upp;
  CHK(darray_size_t_push_back(prims, &prim->iprim) == RES_OK);
}

static void
voxel_commit_radcoefs_range
  (float* vox,
   const enum atrstm_component cpnt,
   const struct radcoefs* radcoefs_min,
   const struct radcoefs* radcoefs_max)
{
  double ka[2], ks[2], kext[2];
  float vox_ka[2], vox_ks[2], vox_kext[2];
  ASSERT(vox);
  ASSERT(radcoefs_min);
  ASSERT(radcoefs_max);

  ka[0] = radcoefs_min->ka;
  ka[1] = radcoefs_max->ka;
  ks[0] = radcoefs_min->ks;
  ks[1] = radcoefs_max->ks;
  kext[0] = radcoefs_min->kext;
  kext[1] = radcoefs_max->kext;

  /* Ensure that the single precision bounds include their double precision
   * version. */
  if(ka[0] != (float)ka[0]) ka[0] = nextafterf((float)ka[0],-FLT_MAX);
  if(ka[1] != (float)ka[1]) ka[1] = nextafterf((float)ka[1], FLT_MAX);
  if(ks[0] != (float)ks[0]) ks[0] = nextafterf((float)ks[0],-FLT_MAX);
  if(ks[1] != (float)ks[1]) ks[1] = nextafterf((float)ks[1], FLT_MAX);
  if(kext[0] != (float)kext[0]) kext[0] = nextafterf((float)kext[0],-FLT_MAX);
  if(kext[1] != (float)kext[1]) kext[1] = nextafterf((float)kext[1], FLT_MAX);

  /* Fetch the range of the voxel optical properties */
  vox_ka[0] = vox_get(vox, cpnt, ATRSTM_RADCOEF_Ka, ATRSTM_SVX_OP_MIN);
  vox_ka[1] = vox_get(vox, cpnt, ATRSTM_RADCOEF_Ka, ATRSTM_SVX_OP_MAX);
  vox_ks[0] = vox_get(vox, cpnt, ATRSTM_RADCOEF_Ks, ATRSTM_SVX_OP_MIN);
  vox_ks[1] = vox_get(vox, cpnt, ATRSTM_RADCOEF_Ks, ATRSTM_SVX_OP_MAX);
  vox_kext[0] = vox_get(vox, cpnt, ATRSTM_RADCOEF_Kext, ATRSTM_SVX_OP_MIN);
  vox_kext[1] = vox_get(vox, cpnt, ATRSTM_RADCOEF_Kext, ATRSTM_SVX_OP_MAX);

  /* Update the range of the voxel optical properties */
  vox_ka[0] = MMIN(vox_ka[0], (float)ka[0]);
  vox_ka[1] = MMAX(vox_ka[1], (float)ka[1]);
  vox_ks[0] = MMIN(vox_ks[0], (float)ks[0]);
  vox_ks[1] = MMAX(vox_ks[1], (float)ks[1]);
  vox_kext[0] = MMIN(vox_kext[0], (float)kext[0]);
  vox_kext[1] = MMAX(vox_kext[1], (float)kext[1]);

  /* Commit the upd to the voxel */
  vox_set(vox, cpnt, ATRSTM_RADCOEF_Ka, ATRSTM_SVX_OP_MIN, vox_ka[0]);
  vox_set(vox, cpnt, ATRSTM_RADCOEF_Ka, ATRSTM_SVX_OP_MAX, vox_ka[1]);
  vox_set(vox, cpnt, ATRSTM_RADCOEF_Ks, ATRSTM_SVX_OP_MIN, vox_ks[0]);
  vox_set(vox, cpnt, ATRSTM_RADCOEF_Ks, ATRSTM_SVX_OP_MAX, vox_ks[1]);
  vox_set(vox, cpnt, ATRSTM_RADCOEF_Kext, ATRSTM_SVX_OP_MIN, vox_kext[0]);
  vox_set(vox, cpnt, ATRSTM_RADCOEF_Kext, ATRSTM_SVX_OP_MAX, vox_kext[1]);
}

static res_T
voxelize_partition
  (struct atrstm* atrstm,
   const struct darray_size_t* prims, /* Primitives intersecting the partition */
   const double part_low[3], /* Lower bound of the partition */
   const double part_upp[3], /* Upper bound of the partition */
   const double vxsz[3], /* Size of a partition voxel */
   const struct atrri_refractive_index* refract_id, /* Refractive index */
   struct part* part)
{
  size_t iprim;
  res_T res = RES_OK;
  ASSERT(atrstm && prims && part_low && part_upp && vxsz && part && refract_id);
  ASSERT(part_low[0] < part_upp[0]);
  ASSERT(part_low[1] < part_upp[1]);
  ASSERT(part_low[2] < part_upp[2]);
  ASSERT(vxsz[0] > 0 && vxsz[1] > 0 && vxsz[2] > 0);

  /* Clean the partition voxels */
  part_clear_voxels(part);

  FOR_EACH(iprim, 0, darray_size_t_size_get(prims)) {
    struct suvm_primitive prim = SUVM_PRIMITIVE_NULL;
    struct suvm_polyhedron poly;
    double poly_low[3];
    double poly_upp[3];
    uint32_t ivxl_low[3];
    uint32_t ivxl_upp[3];
    uint32_t ivxl[3];
    size_t prim_id;

    prim_id = darray_size_t_cdata_get(prims)[iprim];

    /* Retrieve the primitive data and setup its polyhedron */
    SUVM(volume_get_primitive(atrstm->volume, prim_id, &prim));
    SUVM(primitive_setup_polyhedron(&prim, &poly));
    ASSERT(poly.lower[0] <= part_upp[0] && poly.upper[0] >= part_low[0]);
    ASSERT(poly.lower[1] <= part_upp[1] && poly.upper[1] >= part_low[1]);
    ASSERT(poly.lower[2] <= part_upp[2] && poly.upper[2] >= part_low[2]);

    /* Clamp the poly AABB to the partition boundaries */
    poly_low[0] = MMAX(poly.lower[0], part_low[0]);
    poly_low[1] = MMAX(poly.lower[1], part_low[1]);
    poly_low[2] = MMAX(poly.lower[2], part_low[2]);
    poly_upp[0] = MMIN(poly.upper[0], part_upp[0]);
    poly_upp[1] = MMIN(poly.upper[1], part_upp[1]);
    poly_upp[2] = MMIN(poly.upper[2], part_upp[2]);

    /* Transform the polyhedron AABB in partition voxel space */
    ivxl_low[0] = (uint32_t)((poly_low[0] - part_low[0]) / vxsz[0]);
    ivxl_low[1] = (uint32_t)((poly_low[1] - part_low[1]) / vxsz[1]);
    ivxl_low[2] = (uint32_t)((poly_low[2] - part_low[2]) / vxsz[2]);
    ivxl_upp[0] = (uint32_t)ceil((poly_upp[0] - part_low[0]) / vxsz[0]);
    ivxl_upp[1] = (uint32_t)ceil((poly_upp[1] - part_low[1]) / vxsz[1]);
    ivxl_upp[2] = (uint32_t)ceil((poly_upp[2] - part_low[2]) / vxsz[2]);
    ASSERT(ivxl_upp[0] <= PARTITION_DEFINITION);
    ASSERT(ivxl_upp[1] <= PARTITION_DEFINITION);
    ASSERT(ivxl_upp[2] <= PARTITION_DEFINITION);

    /* Iterate over the partition voxels intersected by the primitive AABB */
    FOR_EACH(ivxl[2], ivxl_low[2], ivxl_upp[2]) {
      float vxl_low[3]; /* Voxel lower bound */
      float vxl_upp[3]; /* Voxel upper bound */
      uint64_t mcode[3]; /* Morton code cache */

      vxl_low[2] = (float)((double)ivxl[2] * vxsz[2] + part_low[2]);
      vxl_upp[2] = vxl_low[2] + (float)vxsz[2];
      mcode[2] = morton3D_encode_u21(ivxl[2]) << 0;

      FOR_EACH(ivxl[1], ivxl_low[1], ivxl_upp[1]) {
        vxl_low[1] = (float)((double)ivxl[1] * vxsz[1] + part_low[1]);
        vxl_upp[1] = vxl_low[1] + (float)vxsz[1];
        mcode[1] = (morton3D_encode_u21(ivxl[1]) << 1) | mcode[2];

        FOR_EACH(ivxl[0], ivxl_low[0], ivxl_upp[0]) {
          enum suvm_intersection_type intersect;
          vxl_low[0] = (float)((double)ivxl[0] * vxsz[0] + part_low[0]);
          vxl_upp[0] = vxl_low[0] + (float)vxsz[0];

          /* NOTE mcode[0] store the morton index of the voxel */
          mcode[0] = (morton3D_encode_u21(ivxl[0]) << 2) | mcode[1];

          intersect = suvm_polyhedron_intersect_aabb(&poly, vxl_low, vxl_upp);
          if(intersect != SUVM_INTERSECT_NONE) {
            struct radcoefs radcoefs_min;
            struct radcoefs radcoefs_max;
            float* vox = part_get_voxel(part, mcode[0]);

            /* Currently, gas is not handled */
            radcoefs_min = RADCOEFS_NULL;
            radcoefs_max = RADCOEFS_NULL;
            voxel_commit_radcoefs_range
              (vox, ATRSTM_CPNT_GAS, &radcoefs_min, &radcoefs_max);

            /* Compute the range of the radiative coefficient for the current
             * primitive */
            #ifndef ATRSTM_USE_SIMD
            {
              res = primitive_compute_radcoefs_range
                (atrstm, refract_id, &prim, &radcoefs_min, &radcoefs_max);
              ASSERT(atrstm->use_simd == 0);
            }
            #else
            {
              if(atrstm->use_simd) {
                res = primitive_compute_radcoefs_range_simd4
                  (atrstm, refract_id, &prim, &radcoefs_min, &radcoefs_max);
              } else {
                res = primitive_compute_radcoefs_range
                  (atrstm, refract_id, &prim, &radcoefs_min, &radcoefs_max);
              }
            }
            #endif
            if(UNLIKELY(res != RES_OK)) goto error;
            voxel_commit_radcoefs_range
              (vox, ATRSTM_CPNT_SOOT, &radcoefs_min, &radcoefs_max);
          }
        }
      }
    }
  }

exit:
  return res;
error:
  goto exit;
}

static res_T
voxelize_volumetric_mesh
  (struct atrstm* atrstm,
   const struct atrri_refractive_index* refract_id,
   struct pool* pool)
{
  struct darray_prims_list prims_list; /* Per thread list of primitives */
  const size_t part_def = PARTITION_DEFINITION;
  size_t nparts[3]; /* #partitions along the 3 axis */
  size_t nparts_adjusted;
  double vol_low[3];
  double vol_upp[3];
  double vxsz[3];
  int64_t i;
  int progress = 0;
  ATOMIC nparts_voxelized = 0;
  ATOMIC res = RES_OK;
  ASSERT(atrstm && pool);

  darray_prims_list_init(atrstm->allocator, &prims_list);

  /* Allocate the per thread  primitive lists */
  res = darray_prims_list_resize(&prims_list, atrstm->nthreads);
  if(res != RES_OK) goto error;

  /* Fetch the volume AABB and compute the size of one voxel */
  SUVM(volume_get_aabb(atrstm->volume, vol_low, vol_upp));
  vxsz[0] = (vol_upp[0] - vol_low[0]) / (double)atrstm->grid_max_definition[0];
  vxsz[1] = (vol_upp[1] - vol_low[1]) / (double)atrstm->grid_max_definition[1];
  vxsz[2] = (vol_upp[2] - vol_low[2]) / (double)atrstm->grid_max_definition[2];

  /* Compute the number of partitions */
  nparts[0] = (atrstm->grid_max_definition[0] + (part_def-1)/*ceil*/) / part_def;
  nparts[1] = (atrstm->grid_max_definition[1] + (part_def-1)/*ceil*/) / part_def;
  nparts[2] = (atrstm->grid_max_definition[2] + (part_def-1)/*ceil*/) / part_def;

  /* Compute the overall #partitions allowing Morton indexing */
  nparts_adjusted = MMAX(nparts[0], MMAX(nparts[1], nparts[2]));
  nparts_adjusted = round_up_pow2(nparts_adjusted);
  nparts_adjusted = nparts_adjusted*nparts_adjusted*nparts_adjusted;

  #define LOG_MSG "Voxelizing the volumetric mesh: %3d%%\r"
  log_info(atrstm, LOG_MSG, 0);

  /* Iterate over the partition according to their Morton order and voxelize
   * the primitives that intersect it */
  omp_set_num_threads((int)atrstm->nthreads);
  #pragma omp parallel for schedule(static, 1/*chunk size*/)
  for(i = 0; i < (int64_t)nparts_adjusted; ++i) {
    struct darray_size_t* prims = NULL;
    double part_low[3];
    double part_upp[3];
    uint32_t part_ids[3];
    const int ithread = omp_get_thread_num();
    struct part* part = NULL;
    int pcent;
    size_t n;
    res_T res_local = RES_OK;

    /* Handle error */
    if(ATOMIC_GET(&res) != RES_OK) continue;

    /* Fetch the per thread list of primitives and partition pool */
    prims = darray_prims_list_data_get(&prims_list)+ithread;
    darray_size_t_clear(prims);

    part = pool_next_partition(pool);
    if(!part) { /* An error occurs */
      ATOMIC_SET(&res, RES_UNKNOWN_ERR);
      continue;
    }

    morton_xyz_decode_u21((uint64_t)part->id, part_ids);

    /* Check that the partition is not out of bound due to Morton indexing */
    if(part_ids[0] >= nparts[0]
    || part_ids[1] >= nparts[1]
    || part_ids[2] >= nparts[2]) {
      pool_free_partition(pool, part);
      continue;
    }

    /* Compute the partition AABB */
    part_low[0] = (double)(part_ids[0] * part_def) * vxsz[0] + vol_low[0];
    part_low[1] = (double)(part_ids[1] * part_def) * vxsz[1] + vol_low[1];
    part_low[2] = (double)(part_ids[2] * part_def) * vxsz[2] + vol_low[2];
    part_upp[0] = part_low[0] + (double)part_def * vxsz[0];
    part_upp[1] = part_low[1] + (double)part_def * vxsz[1];
    part_upp[2] = part_low[2] + (double)part_def * vxsz[2];

    /* Find the list of primitives that intersects the partition */
    res_local = suvm_volume_intersect_aabb(atrstm->volume, part_low, part_upp,
      register_primitive, prims);
    if(res_local != RES_OK) {
      ATOMIC_SET(&res, res_local);
      continue;
    }

    res = voxelize_partition
      (atrstm, prims, part_low, part_upp, vxsz, refract_id, part);
    if(res != RES_OK) {
      pool_free_partition(pool, part);
      ATOMIC_SET(&res, res_local);
      continue;
    }

    pool_commit_partition(pool, part);

    /* Update progress message */
    n = (size_t)ATOMIC_INCR(&nparts_voxelized);
    pcent = (int)((n * 100) / (nparts[0]*nparts[1]*nparts[2]));

    #pragma omp critical
    if(pcent > progress) {
      progress = pcent;
      log_info(atrstm, LOG_MSG, pcent);
    }
  }

  if(res != RES_OK) goto error;
  log_info(atrstm, LOG_MSG"\n", 100);

exit:
  darray_prims_list_release(&prims_list);
  return (res_T)res;
error:
  goto exit;
}

static void
voxel_get(const size_t xyz[3], const uint64_t mcode, void* dst, void* context)
{
  struct build_octree_context* ctx = context;
  float* vox = NULL;
  uint64_t ivox, ipart;
  ASSERT(xyz && dst && ctx);
  (void)xyz;

  /* Retrieve the partition morton id and the per partition voxel morton id
   * from the overall voxel morton code. As voxels, partitions are sorted in
   * morton order and thus the partition morton ID is encoded in the MSB of the
   * morton code while the voxels morton ID is stored in its LSB. */
  ipart = (mcode >> (LOG2_PARTITION_DEFINITION*3));
  ivox = (mcode & (BIT_U64(LOG2_PARTITION_DEFINITION*3)-1));

  if(!ctx->part || ctx->part->id != ipart) {
    if(ctx->part) { /* Free the previous partition */
      pool_free_partition(ctx->pool, ctx->part);
    }
    /* Fetch the partition storing the voxel */
    ctx->part = pool_fetch_partition(ctx->pool, ipart);
  }

  if(!ctx->part) { /* An error occurs */
    memset(dst, 0, NFLOATS_PER_VOXEL * sizeof(float));
  } else {
    int cpnt;
    vox = part_get_voxel(ctx->part, ivox); /* Fetch the voxel data */

    /* Setup destination data */
    FOR_EACH(cpnt, 0, ATRSTM_CPNTS_COUNT__) {
      int radcoef;
      FOR_EACH(radcoef, 0, ATRSTM_RADCOEFS_COUNT__) {
        const float k_min = vox_get(vox, cpnt, radcoef, ATRSTM_SVX_OP_MIN);
        const float k_max = vox_get(vox, cpnt, radcoef, ATRSTM_SVX_OP_MAX);
        ASSERT(ATRSTM_SVX_OPS_COUNT__ == 2);

        if(k_min <= k_max) {
          vox_set(dst, cpnt, radcoef, ATRSTM_SVX_OP_MIN, k_min);
          vox_set(dst, cpnt, radcoef, ATRSTM_SVX_OP_MAX, k_max);
        } else {
          /* The voxel was not overlaped by any tetrahedron. Set its radiative
           * coefficients to 0 */
          vox_set(dst, cpnt, radcoef, ATRSTM_SVX_OP_MIN, 0);
          vox_set(dst, cpnt, radcoef, ATRSTM_SVX_OP_MAX, 0);
        }
      }
    }
  }
}

static void
voxels_merge_component
  (void* dst,
   const enum atrstm_component cpnt,
   const void* voxels[],
   const size_t nvoxs)
{
  float ka[2] = {FLT_MAX,-FLT_MAX};
  float ks[2] = {FLT_MAX,-FLT_MAX};
  float kext[2] = {FLT_MAX,-FLT_MAX};
  size_t ivox;
  ASSERT(dst && voxels && nvoxs);

  FOR_EACH(ivox, 0, nvoxs) {
    const float* vox = voxels[ivox];
    ka[0] = MMIN(ka[0], vox_get(vox, cpnt, ATRSTM_RADCOEF_Ka, ATRSTM_SVX_OP_MIN));
    ka[1] = MMAX(ka[1], vox_get(vox, cpnt, ATRSTM_RADCOEF_Ka, ATRSTM_SVX_OP_MAX));
    ks[0] = MMIN(ks[0], vox_get(vox, cpnt, ATRSTM_RADCOEF_Ks, ATRSTM_SVX_OP_MIN));
    ks[1] = MMAX(ks[1], vox_get(vox, cpnt, ATRSTM_RADCOEF_Ks, ATRSTM_SVX_OP_MAX));
    kext[0] = MMIN(kext[0], vox_get(vox, cpnt, ATRSTM_RADCOEF_Kext, ATRSTM_SVX_OP_MIN));
    kext[1] = MMAX(kext[1], vox_get(vox, cpnt, ATRSTM_RADCOEF_Kext, ATRSTM_SVX_OP_MAX));
  }
  vox_set(dst, cpnt, ATRSTM_RADCOEF_Ka, ATRSTM_SVX_OP_MIN, ka[0]);
  vox_set(dst, cpnt, ATRSTM_RADCOEF_Ka, ATRSTM_SVX_OP_MAX, ka[1]);
  vox_set(dst, cpnt, ATRSTM_RADCOEF_Ks, ATRSTM_SVX_OP_MIN, ks[0]);
  vox_set(dst, cpnt, ATRSTM_RADCOEF_Ks, ATRSTM_SVX_OP_MAX, ks[1]);
  vox_set(dst, cpnt, ATRSTM_RADCOEF_Kext, ATRSTM_SVX_OP_MIN, kext[0]);
  vox_set(dst, cpnt, ATRSTM_RADCOEF_Kext, ATRSTM_SVX_OP_MAX, kext[1]);
}

static void
voxels_merge
  (void* dst,
   const void* voxels[],
   const size_t nvoxs,
   void* ctx)
{
  (void)ctx;
  voxels_merge_component(dst, ATRSTM_CPNT_GAS, voxels, nvoxs);
  voxels_merge_component(dst, ATRSTM_CPNT_SOOT, voxels, nvoxs);
}

static INLINE void
voxels_component_compute_kext_range
  (const enum atrstm_component cpnt,
   const struct svx_voxel voxels[],
   const size_t nvoxs,
   float kext[2])
{
  size_t ivox;
  ASSERT(voxels && nvoxs && kext);

  kext[0] = FLT_MAX;
  kext[1] =-FLT_MAX;

  /* Compute the Kext range of the submitted voxels */
  FOR_EACH(ivox, 0, nvoxs) {
    const float* vox = voxels[ivox].data;
    kext[0] = MMIN(kext[0], vox_get(vox, cpnt, ATRSTM_RADCOEF_Kext, ATRSTM_SVX_OP_MIN));
    kext[1] = MMAX(kext[1], vox_get(vox, cpnt, ATRSTM_RADCOEF_Kext, ATRSTM_SVX_OP_MAX));
  }
}

static int
voxels_challenge_merge
  (const struct svx_voxel voxels[],
   const size_t nvoxs,
   void* context)
{
  const struct build_octree_context* ctx = context;
  double lower[3] = { DBL_MAX, DBL_MAX, DBL_MAX};
  double upper[3] = {-DBL_MAX,-DBL_MAX,-DBL_MAX};
  double sz_max;
  size_t ivox;
  float kext_gas[2] = {0, 0};
  float kext_soot[2] = {0, 0};
  int merge_gas = 0;
  int merge_soot = 0;
  ASSERT(voxels && nvoxs && context);

  /* Compute the AABB of the group of voxels and its maximum size */
  FOR_EACH(ivox, 0, nvoxs) {
    lower[0] = MMIN(voxels[ivox].lower[0], lower[0]);
    lower[1] = MMIN(voxels[ivox].lower[1], lower[1]);
    lower[2] = MMIN(voxels[ivox].lower[2], lower[2]);
    upper[0] = MMAX(voxels[ivox].upper[0], upper[0]);
    upper[1] = MMAX(voxels[ivox].upper[1], upper[1]);
    upper[2] = MMAX(voxels[ivox].upper[2], upper[2]);
  }
  sz_max = upper[0] - lower[0];
  sz_max = MMAX(upper[1] - lower[1], sz_max);
  sz_max = MMAX(upper[2] - lower[2], sz_max);

  /* Compute the Kext range for each component */
  voxels_component_compute_kext_range(ATRSTM_CPNT_GAS, voxels, nvoxs, kext_gas);
  voxels_component_compute_kext_range(ATRSTM_CPNT_SOOT, voxels, nvoxs, kext_soot);

  if(kext_gas[0] > kext_gas[1]) { /* Empty voxels */
    /* Always merge empty voxels */
    merge_gas = 1;
    merge_soot = 1;
  } else {
    /* Check if the voxels are candidates to the merge process by evaluating its
     * optical thickness regarding the maximum size of their AABB and a user
     * defined threshold */
    ASSERT(kext_gas[0] <= kext_gas[1]);
    ASSERT(kext_soot[0] <= kext_soot[1]);
    merge_gas = (kext_gas[1] - kext_gas[0])*sz_max <= ctx->tau_threshold;
    merge_soot = (kext_soot[1] - kext_soot[0])*sz_max <= ctx->tau_threshold;
  }
  return merge_gas && merge_soot;
}

static res_T
build_octree
  (struct atrstm* atrstm,
   struct pool* pool)
{
  struct svx_voxel_desc vox_desc = SVX_VOXEL_DESC_NULL;
  struct build_octree_context ctx = BUILD_OCTREE_CONTEXT_NULL;
  double lower[3];
  double upper[3];
  size_t definition[3];
  res_T res = RES_OK;
  ASSERT(atrstm && pool);

  /* Setup the build octree context */
  ctx.atrstm = atrstm;
  ctx.pool = pool;
  ctx.tau_threshold = atrstm->optical_thickness;

  /* Setup the voxel descriptor. TODO in shortwave, the NFLOATS_PER_VOXEL
   * could be divided by 2 since there is no gas to handle */
  vox_desc.get = voxel_get;
  vox_desc.merge = voxels_merge;
  vox_desc.challenge_merge = voxels_challenge_merge;
  vox_desc.context = &ctx;
  vox_desc.size = NFLOATS_PER_VOXEL * sizeof(float);

  SUVM(volume_get_aabb(atrstm->volume, lower, upper));

  /* Create the octree */
  definition[0] = (size_t)atrstm->grid_max_definition[0];
  definition[1] = (size_t)atrstm->grid_max_definition[1];
  definition[2] = (size_t)atrstm->grid_max_definition[2];
  res = svx_octree_create
    (atrstm->svx, lower, upper, definition, &vox_desc, &atrstm->octree);
  if(res != RES_OK) goto error;

exit:
  return res;
error:
  goto exit;
}

static res_T
build_octrees(struct atrstm* atrstm)
{
  struct atrri_refractive_index refract_id = ATRRI_REFRACTIVE_INDEX_NULL;
  struct pool pool;
  double wlen;
  int pool_is_init = 0;
  ATOMIC res = RES_OK;
  ASSERT(atrstm && check_octrees_parameters(atrstm));

  /* Currently only shortwave is handled and in this case, radiative
   * transfert is monochromatic */
  ASSERT(atrstm->spectral_type == ATRSTM_SPECTRAL_SW);
  ASSERT(atrstm->wlen_range[0] == atrstm->wlen_range[1]);
  wlen = atrstm->wlen_range[0];

  /* Fetch the submitted wavelength */
  res = atrri_fetch_refractive_index(atrstm->atrri, wlen, &refract_id);
  if(res != RES_OK) goto error;

  /* Initialise the pool of partitions */
  res = pool_init(atrstm->allocator, 16 * atrstm->nthreads, &pool);
  if(res != RES_OK) {
    log_err(atrstm,
      "Error initializing the pool of voxel partitions -- %s.\n",
      res_to_cstr((res_T)res));
    goto error;
  }
  pool_is_init = 1;

  log_info(atrstm,
    "Evaluate and partition the field of optical properties.\n");
  log_info(atrstm,
    "Grid definition: %ux%ux%u.\n", SPLIT3(atrstm->grid_max_definition));

  omp_set_nested(1); /* Enable nested threads for voxelize_volumetric_mesh */
  #pragma omp parallel sections num_threads(2)
  {
    #pragma omp section
    {
      const res_T res_local = voxelize_volumetric_mesh
        (atrstm, &refract_id, &pool);
      if(res_local != RES_OK) {
        log_err(atrstm, "Error voxelizing the volumetric mesh -- %s\n",
          res_to_cstr(res_local));
        pool_invalidate(&pool);
        ATOMIC_SET(&res, res_local);
      }
    }

    #pragma omp section
    {
      const res_T res_local = build_octree(atrstm, &pool);
      if(res_local != RES_OK) {
        log_err(atrstm, "Error building the octree -- %s\n",
          res_to_cstr(res_local));
        pool_invalidate(&pool);
        ATOMIC_SET(&res, res_local);
      }
    }
  }
  if(res != RES_OK) goto error;

  if(atrstm->cache) {
    ASSERT(cache_is_empty(atrstm->cache));

    log_info(atrstm, "Write acceleration data structure to '%s'.\n",
      cache_get_name(atrstm->cache));

    res = svx_tree_write(atrstm->octree, cache_get_stream(atrstm->cache));
    if(res != RES_OK) {
      log_err(atrstm, "Could not write the octrees to the cache -- %s.\n",
        res_to_cstr((res_T)res));
      goto error;
    }

    fflush(cache_get_stream(atrstm->cache));
  }

exit:
  if(pool_is_init) pool_release(&pool);
  return (res_T)res;
error:
  goto exit;
}

static res_T
load_octrees(struct atrstm* atrstm)
{
  FILE* stream = NULL;
  res_T res = RES_OK;
  ASSERT(atrstm && atrstm->cache && !cache_is_empty(atrstm->cache));

  log_info(atrstm, "Load the acceleration data structures of the optical "
    "properties from '%s'.\n", cache_get_name(atrstm->cache));

  stream = cache_get_stream(atrstm->cache);
  res = svx_tree_create_from_stream(atrstm->svx, stream, &atrstm->octree);
  if(res != RES_OK) {
    log_err(atrstm, "Could not load the acceleration data structures -- %s.\n",
      res_to_cstr(res));
    goto error;
  }

exit:
  return res;
error:
  goto exit;
}

/*******************************************************************************
 * Local functions
 ******************************************************************************/
res_T
setup_octrees(struct atrstm* atrstm)
{
  char buf[128];
  struct time t0, t1;
  size_t sz;
  res_T res = RES_OK;
  ASSERT(atrstm);

  time_current(&t0);

  if(atrstm->cache && !cache_is_empty(atrstm->cache)) {
    res = load_octrees(atrstm);;
    if(res != RES_OK) goto error;
  } else {
    res = build_octrees(atrstm);
    if(res != RES_OK) goto error;
  }

  time_sub(&t0, time_current(&t1), &t0);
  time_dump(&t0, TIME_ALL, NULL, buf, sizeof(buf));
  log_info(atrstm, "Setup the partitionning data structures in %s.\n", buf);

  sz = MEM_ALLOCATED_SIZE(&atrstm->svx_allocator);
  dump_memory_size(sz, NULL, buf, sizeof(buf));
  log_info(atrstm, "Star-VoXel memory usage: %s.\n", buf);

exit:
  return res;
error:
  goto exit;
}

void
octrees_clean(struct atrstm* atrstm)
{
  ASSERT(atrstm);
  if(atrstm->octree) SVX(tree_ref_put(atrstm->octree));
}

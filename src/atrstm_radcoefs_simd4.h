/* Copyright (C) 2022, 2023 |Méso|Star> (contact@meso-star.com)
 * Copyright (C) 2020, 2021 Centre National de la Recherche Scientifique
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef ATRSTM_RADCOEFS_SIMD4_H
#define ATRSTM_RADCOEFS_SIMD4_H

#include "atrstm.h"
#include "atrstm_radcoefs.h"
#include "atrstm_rdgfa_simd4.h"

#include <rsimd/math.h>
#include <rsimd/rsimd.h>

#include <rsys/math.h>
#include <rsys/rsys.h>

/* Radiative coefficients in m^-1 */
struct radcoefs_simd4 {
  v4f_T ka; /* Absorption coefficient */
  v4f_T ks; /* Scattering coefficient */
  v4f_T kext; /* Extinction coefficient */
};
static const struct radcoefs_simd4 RADCOEFS_SIMD4_NULL;

struct radcoefs_compute_simd4_args {
  v4f_T lambda; /* wavelength [nm] */
  v4f_T n; /* Refractive index (real component) */
  v4f_T kappa; /* Refractive index (imaginary component) */
  v4f_T fractal_prefactor;
  v4f_T fractal_dimension;
  v4f_T soot_volumic_fraction; /* In m^3(soot) / m^3 */
  v4f_T soot_primary_particles_count;
  v4f_T soot_primary_particles_diameter; /* In nm */
  int radcoefs_mask; /* Combination of atrstm_radcoef_flag */
};
static const struct radcoefs_compute_simd4_args RADCOEFS_COMPUTE_SIMD4_ARGS_NULL;

/* Forward declarations */
struct atrstm;
struct atrri_refractive_index;
struct suvm_primitive;

extern LOCAL_SYM res_T
fetch_radcoefs_simd4
  (const struct atrstm* atrstm,
   const struct atrstm_fetch_radcoefs_args* args,
   atrstm_radcoefs_T radcoefs);

extern LOCAL_SYM res_T
primitive_compute_radcoefs_simd4
  (const struct atrstm* atrstm,
   const struct atrri_refractive_index* refract_id,
   const struct suvm_primitive* prim,
   const int radcoefs_mask,
   struct radcoefs_simd4* radcoefs); /* Per node radiative coefficients */

static INLINE res_T
primitive_compute_radcoefs_range_simd4
  (const struct atrstm* atrstm,
   const struct atrri_refractive_index* refract_id,
   const struct suvm_primitive* prim,
   struct radcoefs* radcoefs_min,
   struct radcoefs* radcoefs_max)
{
  struct radcoefs_simd4 radcoefs;
  v4f_T ka[2];
  v4f_T ks[2];
  v4f_T kext[2];
  res_T res = RES_OK;
  ASSERT(radcoefs_min && radcoefs_max);

  res = primitive_compute_radcoefs_simd4
    (atrstm, refract_id, prim, ATRSTM_RADCOEFS_MASK_ALL, &radcoefs);
  if(res != RES_OK) return res;

  ka[0] = v4f_reduce_min(radcoefs.ka);
  ka[1] = v4f_reduce_max(radcoefs.ka);
  ks[0] = v4f_reduce_min(radcoefs.ks);
  ks[1] = v4f_reduce_max(radcoefs.ks);
  kext[0] = v4f_reduce_min(radcoefs.kext);
  kext[1] = v4f_reduce_max(radcoefs.kext);

  radcoefs_min->ka = v4f_x(ka[0]);
  radcoefs_max->ka = v4f_x(ka[1]);
  radcoefs_min->ks = v4f_x(ks[0]);
  radcoefs_max->ks = v4f_x(ks[1]);
  radcoefs_min->kext = v4f_x(kext[0]);
  radcoefs_max->kext = v4f_x(kext[1]);

  return RES_OK;
}

/* SIMD version of the radcoefs_compute function. Refer to its scalar version
 * for informations of what it does */
static INLINE void
radcoefs_compute_simd4
  (struct radcoefs_simd4* radcoefs,
   const struct radcoefs_compute_simd4_args* args)
{
  /* Constant */
  const v4f_T pi = v4f_set1((float)PI);

  /* Input variables */
  const v4f_T lambda = args->lambda; /* [nm] */
  const v4f_T n = args->n;
  const v4f_T kappa = args->kappa;
  const v4f_T Fv = args->soot_volumic_fraction; /* [nm^3(soot)/nm] */
  const v4f_T Np = args->soot_primary_particles_count;
  const v4f_T Dp = args->soot_primary_particles_diameter; /* [nm] */
  const v4f_T kf = args->fractal_prefactor;
  const v4f_T Df = args->fractal_dimension;
  const int ka = (args->radcoefs_mask & ATRSTM_RADCOEF_FLAG_Ka) != 0;
  const int ks = (args->radcoefs_mask & ATRSTM_RADCOEF_FLAG_Ks) != 0;
  const int kext = (args->radcoefs_mask & ATRSTM_RADCOEF_FLAG_Kext) != 0;

  /* SIMD mask */
  const v4f_T is_Fv_not_null = v4f_neq(Fv, v4f_zero());
  const v4f_T is_Np_not_null = v4f_neq(Np, v4f_zero());
  const v4f_T query_radcoefs =
    v4f_mask1(args->radcoefs_mask & ATRSTM_RADCOEFS_MASK_ALL ? ~0 : 0);
  const v4f_T mask =
    v4f_and(v4f_and(is_Fv_not_null, is_Np_not_null), query_radcoefs);

  /* Clean up radiative coefficients */
  radcoefs->ka = v4f_zero();
  radcoefs->ks = v4f_zero();
  radcoefs->kext = v4f_zero();

  if(v4f_movemask(mask) != 0) {
    /* Precomputed values */
    const v4f_T n2 = v4f_mul(n, n);
    const v4f_T kappa2 = v4f_mul(kappa, kappa);
    const v4f_T four_n2_kappa2 = v4f_mul(v4f_set1(4), v4f_mul(n2, kappa2));
    const v4f_T xp = v4f_div(v4f_mul(pi, Dp), lambda);
    const v4f_T xp3 = v4f_mul(v4f_mul(xp, xp), xp);
    const v4f_T k = v4f_div(v4f_mul(v4f_set1(2), pi), lambda);
    const v4f_T k2 = v4f_mul(k, k);

    const v4f_T Dp3 = v4f_mul(v4f_mul(Dp, Dp), Dp);
    const v4f_T Np_pi_Dp3 = v4f_mul(v4f_mul(Np, pi), Dp3);
    const v4f_T Va = v4f_div(Np_pi_Dp3, v4f_set1(6)); /* [nm^3] */
    const v4f_T rho = v4f_div(Fv, Va); /* [#aggregates / nm^3] */
    const v4f_T tmp0 = v4f_mul(v4f_set1(1e9f), rho);

    /* E(m), m = n + i*kappa */
    const v4f_T tmp1 = v4f_add(v4f_sub(n2, kappa2), v4f_set1(2));
    const v4f_T denom = v4f_madd(tmp1, tmp1, four_n2_kappa2);
    const v4f_T six_n_kappa = v4f_mul(v4f_mul(v4f_set1(6), n), kappa);
    const v4f_T Em = v4f_div(six_n_kappa, denom);

    if(ka || kext) {
      /* Absorption cross section, [nm^3/aggrefate] */
      const v4f_T four_pi_xp3 = v4f_mul(v4f_mul(v4f_set1(4), pi), xp3);
      const v4f_T Cabs = v4f_mul(v4f_mul(Np, v4f_div(four_pi_xp3, k2)), Em);

      /* Absorption coefficient, [m^-1] */
      radcoefs->ka = v4f_and(v4f_mul(tmp0, Cabs), mask);
    }

    if(ks || kext) {
      /* F(m), m = n + i*kappa */
      const v4f_T n2_sub_kappa2 = v4f_sub(n2, kappa2);
      const v4f_T n2_sub_kappa2_sub_1 = v4f_sub(n2_sub_kappa2, v4f_set1(1));
      const v4f_T n2_sub_kappa2_add_2 = v4f_add(n2_sub_kappa2, v4f_set1(2));
      const v4f_T tmp2 = v4f_mul(n2_sub_kappa2_sub_1, n2_sub_kappa2_add_2);
      const v4f_T tmp3 = v4f_div(v4f_add(tmp2, four_n2_kappa2), denom);
      const v4f_T Fm = v4f_madd(tmp3, tmp3, v4f_mul(Em, Em));

      /* Gyration radius */
      const v4f_T Rg = compute_gyration_radius_simd4(kf, Df, Np, Dp); /* [nm] */
      const v4f_T Rg2 = v4f_mul(Rg, Rg);
      const v4f_T four_k2_Rg2 = v4f_mul(v4f_mul(v4f_set1(4), k2), Rg2);
      const v4f_T tmp4 = v4f_div(four_k2_Rg2, v4f_mul(v4f_set1(3), Df));
      const v4f_T tmp5 = v4f_add(v4f_set1(1), tmp4);
      const v4f_T g = v4f_pow(tmp5, v4f_mul(Df, v4f_set1(-0.5f)));

      /* Scattering cross section, [nm^3/aggrefate] */
      const v4f_T Np2 = v4f_mul(Np, Np);
      const v4f_T xp6 = v4f_mul(xp3, xp3);
      const v4f_T eight_pi_xp6 = v4f_mul(v4f_mul(v4f_set1(8), pi), xp6);
      const v4f_T tmp7 = v4f_div(eight_pi_xp6, v4f_mul(v4f_set1(3), k2));
      const v4f_T Csca = v4f_mul(v4f_mul(v4f_mul(Np2, tmp7), Fm), g);

      /* Scattering coefficient, [m^-1] */
      radcoefs->ks = v4f_and(v4f_mul(tmp0, Csca), mask);
    }

    if(kext) {
      radcoefs->kext = v4f_add(radcoefs->ka, radcoefs->ks);
    }

    ASSERT(v4f_movemask(mask) && v4f_movemask(v4f_neq(denom, v4f_zero())));
    ASSERT(v4f_movemask(mask) && v4f_movemask(v4f_neq(Df, v4f_zero())));
    ASSERT(v4f_movemask(mask) && v4f_movemask(v4f_neq(Dp, v4f_zero())));
  }
}

#endif /* ATRSTM_RADCOEFS_SIMD4_H */

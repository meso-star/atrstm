/* Copyright (C) 2022, 2023 |Méso|Star> (contact@meso-star.com)
 * Copyright (C) 2020, 2021 Centre National de la Recherche Scientifique
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#define _POSIX_C_SOURCE 200112L /* fdopen */

#include "atrstm_c.h"
#include "atrstm_cache.h"
#include "atrstm_log.h"

#include <astoria/atrri.h>
#include <astoria/atrtp.h>

#include <rsys/cstr.h>
#include <rsys/hash.h>
#include <rsys/mem_allocator.h>
#include <rsys/ref_count.h>
#include <rsys/str.h>

#include <star/suvm.h>

#include <errno.h>
#include <fcntl.h> /* open */
#include <sys/stat.h> /* S_IRUSR & S_IWUSR */
#include <unistd.h> /* close */
#include <string.h>

struct cache {
  FILE* stream;
  struct atrstm* atrstm;
  struct str name;
  int empty;
  ref_T ref;
};

struct hash {
  hash256_T therm_props;
  hash256_T refract_ids;
  hash256_T volume;
};

/* Current version the cache memory layout. One should increment it and perform
 * a version management onto serialized data when the cache data structure is
 * updated. */
static const int CACHE_VERSION = 1;

/*******************************************************************************
 * Helper functions
 ******************************************************************************/
static res_T
hash_compute(struct atrstm* atrstm, struct hash* hash)
{
  struct atrtp_desc atrtp_desc = ATRTP_DESC_NULL;
  struct atrri_desc atrri_desc = ATRRI_DESC_NULL;
  res_T res = RES_OK;
  ASSERT(atrstm && hash);

  res = atrtp_get_desc(atrstm->atrtp, &atrtp_desc);
  if(res != RES_OK) goto error;
  res = atrri_get_desc(atrstm->atrri, &atrri_desc);
  if(res != RES_OK) goto error;

  hash_sha256
    (atrtp_desc.properties,
     atrtp_desc.nnodes*sizeof(double[ATRTP_COUNT__]),
     hash->therm_props);

  hash_sha256
    (atrri_desc.indices,
     atrri_desc.nindices*sizeof(struct atrri_refractive_index),
     hash->refract_ids);

  res = suvm_volume_compute_hash
    (atrstm->volume, SUVM_POSITIONS|SUVM_INDICES, hash->volume);
  if(res != RES_OK) goto error;

exit:
  return res;
error:
  log_err(atrstm, "Error computing the AtrSTM hash -- %s.\n",
    res_to_cstr(res));
  goto exit;
}

static res_T
hash_write(const struct hash* hash, FILE* fp)
{
  res_T res = RES_OK;
  ASSERT(hash && fp);

  #define WRITE(Var, N) {                                                      \
    if(fwrite((Var), sizeof(*(Var)), (N), fp) != (N)) {                        \
      res = RES_IO_ERR;                                                        \
      goto error;                                                              \
    }                                                                          \
  } (void)0
  WRITE(hash->therm_props, sizeof(hash256_T));
  WRITE(hash->refract_ids, sizeof(hash256_T));
  WRITE(hash->volume, sizeof(hash256_T));
  #undef WRITE

exit:
  return res;
error:
  goto exit;
}

static res_T
hash_read(struct hash* hash, FILE* fp)
{
  res_T res = RES_OK;
  ASSERT(hash && fp);

  #define READ(Var, N) {                                                       \
    if(fread((Var), sizeof(*(Var)), (N), fp) != (N)) {                         \
      if(feof(fp)) {                                                           \
        res = RES_BAD_ARG;                                                     \
      } else if(ferror(fp)) {                                                  \
        res = RES_IO_ERR;                                                      \
      } else {                                                                 \
        res = RES_UNKNOWN_ERR;                                                 \
      }                                                                        \
      goto error;                                                              \
    }                                                                          \
  } (void)0
  READ(hash->therm_props, sizeof(hash256_T));
  READ(hash->refract_ids, sizeof(hash256_T));
  READ(hash->volume, sizeof(hash256_T));
  #undef READ

exit:
  return res;
error:
  goto exit;
}

/* Setup the cache header, i.e. data that uniquely identify the cache regarding
 * the input data */
static res_T
write_cache_header(struct cache* cache)
{
  struct hash hash;
  res_T res = RES_OK;
  ASSERT(cache);

  #define WRITE(Var, N) {                                                      \
    if(fwrite((Var), sizeof(*(Var)), (N), cache->stream) != (N)) {             \
      log_err(cache->atrstm,                                                   \
        "%s: could not write the cache header.\n", cache_get_name(cache));     \
      res = RES_IO_ERR;                                                        \
      goto error;                                                              \
    }                                                                          \
  } (void)0
  WRITE(&CACHE_VERSION, 1);
  WRITE(&cache->atrstm->fractal_prefactor, 1);
  WRITE(&cache->atrstm->fractal_dimension, 1);
  WRITE(&cache->atrstm->spectral_type, 1);
  WRITE(cache->atrstm->wlen_range, 2);
  WRITE(cache->atrstm->grid_max_definition, 3);
  WRITE(&cache->atrstm->optical_thickness, 1);
  WRITE(&cache->atrstm->use_simd, 1);
  #undef WRITE

  res = hash_compute(cache->atrstm, &hash);
  if(res != RES_OK) goto error;
  res = hash_write(&hash, cache->stream);
  if(res != RES_OK) goto error;

exit:
  return res;
error:
  goto exit;
}

static res_T
read_cache_header(struct cache* cache)
{
  struct hash cached_hash;
  struct hash current_hash;
  double fractal_prefactor = 0;
  double fractal_dimension = 0;
  enum atrstm_spectral_type spectral_type = ATRSTM_SPECTRAL_TYPES_COUNT__;
  double wlen_range[2] = {0,0};
  unsigned grid_max_definition[3] = {0,0,0};
  double optical_thickness = 0;
  int use_simd = 0;
  int cache_version = 0;
  res_T res = RES_OK;
  ASSERT(cache);

  /* Read the cache header */
  #define READ(Var, N) {                                                       \
    if(fread((Var), sizeof(*(Var)), (N), cache->stream) != (N)) {              \
      if(feof(cache->stream)) {                                                \
        res = RES_BAD_ARG;                                                     \
      } else if(ferror(cache->stream)) {                                       \
        res = RES_IO_ERR;                                                      \
      } else {                                                                 \
        res = RES_UNKNOWN_ERR;                                                 \
      }                                                                        \
      log_err(cache->atrstm, "%s: could not read the cache header -- %s.\n",   \
        cache_get_name(cache), res_to_cstr(res));                              \
      goto error;                                                              \
    }                                                                          \
  } (void)0

  READ(&cache_version, 1);
  if(cache_version != CACHE_VERSION) {
    log_err(cache->atrstm,
      "%s: invalid cache in version %d. Expecting a cache in version %d.\n",
      cache_get_name(cache), cache_version, CACHE_VERSION);
    res = RES_BAD_ARG;
    goto error;
  }
  #define CHK_VAR(CachedVal, CurrentVal, Name, Fmt) {                          \
    if((CachedVal) != (CurrentVal)) {                                          \
      log_err(cache->atrstm,                                                   \
        "%s: invalid cache regarding the "Name". "                             \
        "Cached value: "Fmt". Current Value: "Fmt".\n",                        \
        cache_get_name(cache), (CachedVal), (CurrentVal));                     \
      res = RES_BAD_ARG;                                                       \
      goto error;                                                              \
    }                                                                          \
  } (void)0

  READ(&fractal_prefactor, 1);
  CHK_VAR(fractal_prefactor, cache->atrstm->fractal_prefactor,
    "fractal prefactor", "%g");

  READ(&fractal_dimension, 1);
  CHK_VAR(fractal_dimension, cache->atrstm->fractal_dimension,
    "fractal dimension", "%g");

  READ(&spectral_type, 1);
  CHK_VAR(spectral_type, cache->atrstm->spectral_type,
    "spectral type", "%i");

  READ(wlen_range, 2);
  CHK_VAR(wlen_range[0], cache->atrstm->wlen_range[0],
    "spectral lower bound", "%g");
  CHK_VAR(wlen_range[1], cache->atrstm->wlen_range[1],
    "spectral upper bound", "%g");

  READ(grid_max_definition, 3);
  CHK_VAR(grid_max_definition[0], cache->atrstm->grid_max_definition[0],
    "grid X definition", "%u");
  CHK_VAR(grid_max_definition[1], cache->atrstm->grid_max_definition[1],
    "grid Y definition", "%u");
  CHK_VAR(grid_max_definition[2], cache->atrstm->grid_max_definition[2],
    "grid Z definition", "%u");

  READ(&optical_thickness, 1);
  CHK_VAR(optical_thickness, cache->atrstm->optical_thickness,
    "optical thickness", "%g");

  READ(&use_simd, 1);
  CHK_VAR(use_simd, cache->atrstm->use_simd, "use_simd flag", "%i");

  #undef CHK_VAR

  res = hash_read(&cached_hash, cache->stream);
  if(res != RES_OK) goto error;
  res = hash_compute(cache->atrstm, &current_hash);
  if(res != RES_OK) goto error;

  #define CHK_HASH(CachedHash, CurrentHash, Name) {                            \
    if(!hash256_eq((CachedHash), (CurrentHash))) {                             \
      log_err(cache->atrstm,                                                   \
        "%s: invalid cache regarding the submitted "Name".\n",                 \
        cache_get_name(cache));                                                \
      res = RES_BAD_ARG;                                                       \
      goto error;                                                              \
    }                                                                          \
  } (void)0
  CHK_HASH(cached_hash.therm_props, current_hash.therm_props,
    "thermodynamic properties");
  CHK_HASH(cached_hash.refract_ids, current_hash.refract_ids,
    "refractive indices");
  CHK_HASH(cached_hash.volume, current_hash.volume,
    "volumetric mesh");
  #undef CHK_HASH

exit:
  return res;
error:
  goto exit;
}

static void
release_cache(ref_T* ref)
{
  struct cache* cache = NULL;
  struct atrstm* atrstm = NULL;
  ASSERT(ref);
  cache = CONTAINER_OF(ref, struct cache, ref);
  atrstm = cache->atrstm;
  if(cache->stream) CHK(fclose(cache->stream) == 0);
  str_release(&cache->name);
  MEM_RM(atrstm->allocator, cache);
}

/*******************************************************************************
 * Local functions
 ******************************************************************************/
res_T
cache_create
  (struct atrstm* atrstm,
   const char* filename,
   struct cache** out_cache)
{
  struct cache* cache = NULL;
  struct stat cache_stat;
  int fd = -1;
  int err = 0;
  res_T res = RES_OK;
  ASSERT(atrstm && filename && out_cache);

  cache = MEM_CALLOC(atrstm->allocator, 1, sizeof(*cache));
  if(!cache) {
    res = RES_MEM_ERR;
    goto error;
  }
  ref_init(&cache->ref);
  str_init(atrstm->allocator, &cache->name);
  cache->atrstm = atrstm;

  res = str_set(&cache->name, filename);
  if(res != RES_OK) {
    log_err(atrstm, "Could not copy the cache filename -- %s.\n",
      res_to_cstr(res));
    goto error;
  }

  fd = open(filename, O_CREAT|O_RDWR, S_IRUSR|S_IWUSR);
  if(fd < 0) {
    log_err(atrstm, "Could not open the cache file `%s' -- %s.\n",
      filename, strerror(errno));
    res = RES_IO_ERR;
    goto error;
  }

  cache->stream = fdopen(fd, "w+");
  if(!cache->stream) {
    log_err(atrstm, "Could not open the cache file `%s' -- %s.\n",
      filename, strerror(errno));
    res = RES_IO_ERR;
    goto error;
  }
  fd = -1;

  err = stat(filename, &cache_stat);
  if(err < 0) {
    log_err(atrstm, "Could not stat the cache file `%s' -- %s.\n",
      filename, strerror(errno));
    res = RES_IO_ERR;
    goto error;
  }

  if(cache_stat.st_size == 0) { /* Empty cache */
    cache->empty = 1;
    res = write_cache_header(cache);
    if(res != RES_OK) goto error;
  } else { /* Cache already exists */
    cache->empty = 0;
    res = read_cache_header(cache);
    if(res != RES_OK) goto error;
  }

exit:
  *out_cache = cache;
  return res;
error:
  if(cache) { cache_ref_put(cache); cache = NULL; }
  if(fd >= 0) CHK(close(fd) == 0);
  goto exit;
}

void
cache_ref_get(struct cache* cache)
{
  ASSERT(cache);
  ref_get(&cache->ref);
}

void
cache_ref_put(struct cache* cache)
{
  ASSERT(cache);
  ref_put(&cache->ref, release_cache);
}

FILE*
cache_get_stream(struct cache* cache)
{
  ASSERT(cache);
  return cache->stream;
}

int
cache_is_empty(const struct cache* cache)
{
  ASSERT(cache);
  return cache->empty;
}

const char*
cache_get_name(const struct cache* cache)
{
  ASSERT(cache);
  return str_cget(&cache->name);
}

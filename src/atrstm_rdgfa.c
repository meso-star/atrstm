/* Copyright (C) 2022, 2023 |Méso|Star> (contact@meso-star.com)
 * Copyright (C) 2020, 2021 Centre National de la Recherche Scientifique
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "atrstm_c.h"
#include "atrstm_rdgfa.h"

#include <astoria/atrtp.h>

/*******************************************************************************
 * Helper function
 ******************************************************************************/
static INLINE int
check_fetch_rdgfa_args
  (const struct atrstm* atrstm,
   const struct atrstm_fetch_rdgfa_args* args)
{
  return args
      && !SUVM_PRIMITIVE_NONE(&args->prim)
      && args->wavelength >= atrstm->wlen_range[0]
      && args->wavelength <= atrstm->wlen_range[1];
}

/*******************************************************************************
 * Exported functions
 ******************************************************************************/
res_T
atrstm_fetch_rdgfa
  (const struct atrstm* atrstm,
   const struct atrstm_fetch_rdgfa_args* args,
   struct atrstm_rdgfa* rdgfa)
{
  struct atrtp_desc atrtp_desc = ATRTP_DESC_NULL;
  double soot_primary_particles_count[4];
  double soot_primary_particles_diameter[4];
  double Np;
  double Dp;
  int inode;
  res_T res = RES_OK;

  if(!atrstm || !check_fetch_rdgfa_args(atrstm, args) || !rdgfa) {
    res = RES_BAD_ARG;
    goto error;
  }

  res = atrtp_get_desc(atrstm->atrtp, &atrtp_desc);
  if(res != RES_OK) goto error;

  FOR_EACH(inode, 0, 4) {
    const double* node;

    /* Fetch the thermodynamic properties of the node */
    node = atrtp_desc_get_node_properties(&atrtp_desc, args->prim.indices[inode]);

    /* Fetch the required node attributes */
    soot_primary_particles_count[inode] = 
      node[ATRTP_SOOT_PRIMARY_PARTICLES_COUNT];
    soot_primary_particles_diameter[inode] = 
      node[ATRTP_SOOT_PRIMARY_PARTICLES_DIAMETER];
  }

  /* Linearly interpolate the node attributes */
  Np = 
    soot_primary_particles_count[0] * args->bcoords[0]
  + soot_primary_particles_count[1] * args->bcoords[1]
  + soot_primary_particles_count[2] * args->bcoords[2]
  + soot_primary_particles_count[3] * args->bcoords[3];
  Dp = 
    soot_primary_particles_diameter[0] * args->bcoords[0]
  + soot_primary_particles_diameter[1] * args->bcoords[1]
  + soot_primary_particles_diameter[2] * args->bcoords[2]
  + soot_primary_particles_diameter[3] * args->bcoords[3];

  /* Setup output data */
  rdgfa->wavelength = args->wavelength;
  rdgfa->fractal_dimension = atrstm->fractal_dimension;
  rdgfa->gyration_radius = compute_gyration_radius
    (atrstm->fractal_prefactor, atrstm->fractal_dimension, Np, Dp);

exit:
  return res;
error:
  goto exit;
}


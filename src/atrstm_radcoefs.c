/* Copyright (C) 2022, 2023 |Méso|Star> (contact@meso-star.com)
 * Copyright (C) 2020, 2021 Centre National de la Recherche Scientifique
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "atrstm_c.h"
#include "atrstm_log.h"
#include "atrstm_radcoefs.h"

#ifdef ATRSTM_USE_SIMD
  #include "atrstm_radcoefs_simd4.h"
#endif

#include <astoria/atrri.h>
#include <astoria/atrtp.h>

#include <star/suvm.h>

#include <rsys/cstr.h>

#include <string.h>

/*******************************************************************************
 * Exported functions
 ******************************************************************************/
res_T
atrstm_fetch_radcoefs
  (const struct atrstm* atrstm,
   const struct atrstm_fetch_radcoefs_args* args,
   atrstm_radcoefs_T radcoefs)
{
  res_T res = RES_OK;

  #ifndef ATRSTM_USE_SIMD
  {
    ASSERT(atrstm->use_simd == 0);
    res = fetch_radcoefs(atrstm, args, radcoefs);
    if(res != RES_OK) goto error;
  }
  #else
  {
    if(!atrstm->use_simd) {
      res = fetch_radcoefs(atrstm, args, radcoefs);
      if(res != RES_OK) goto error;
    } else {
      res = fetch_radcoefs_simd4(atrstm, args, radcoefs);
      if(res != RES_OK) goto error;
    }
  }
  #endif /* ATRSTM_USE_SIMD */

exit:
  return res;
error:
  goto exit;
}

/*******************************************************************************
 * Local functions functions
 ******************************************************************************/
int
check_fetch_radcoefs_args
  (const struct atrstm* atrstm,
   const struct atrstm_fetch_radcoefs_args* args,
   const char* func_name)
{
  int i;
  ASSERT(atrstm && args && func_name);

  if(!args
  || SUVM_PRIMITIVE_NONE(&args->prim)
  || args->wavelength < atrstm->wlen_range[0]
  || args->wavelength > atrstm->wlen_range[1]
  || !(args->radcoefs_mask & ATRSTM_RADCOEFS_MASK_ALL)
  || !(args->components_mask & ATRSTM_CPNTS_MASK_ALL))
    return 0;

  if(atrstm->spectral_type != ATRSTM_SPECTRAL_SW) {
    log_err(atrstm, "%s: only shortwave is currently supported.\n",
      func_name);
    return 0;
  }

  FOR_EACH(i, 0, ATRSTM_RADCOEFS_COUNT__) {
    if(args->radcoefs_mask & BIT(i)) {
      if(args->k_min[i] > args->k_max[i]) return 0;
    }
  }

  return 1;
}

res_T
fetch_radcoefs
  (const struct atrstm* atrstm,
   const struct atrstm_fetch_radcoefs_args* args,
   atrstm_radcoefs_T radcoefs)
{
  struct radcoefs prim_radcoefs[4];
  res_T res = RES_OK;

  if(!atrstm
  || !check_fetch_radcoefs_args(atrstm, args, FUNC_NAME)
  || !radcoefs) {
    res = RES_BAD_ARG;
    goto error;
  }
  memset(radcoefs, 0, sizeof(double[ATRSTM_RADCOEFS_COUNT__]));

  /* Compute the radiative properties of the primitive nodes */
  res = primitive_compute_radcoefs(atrstm, &atrstm->refract_id, &args->prim,
    args->radcoefs_mask, prim_radcoefs);
  if(res != RES_OK) {
    log_err(atrstm,
      "Error computing the radiative properties for the primitive %lu -- %s.\n",
      (unsigned long)args->prim.iprim,
      res_to_cstr(res));
    goto error;
  }

  /* Linearly interpolate the node radiative properties */
  if(args->radcoefs_mask & ATRSTM_RADCOEF_FLAG_Ka) {
    const double prim_ka_min = MMIN
      (MMIN(prim_radcoefs[0].ka, prim_radcoefs[1].ka),
       MMIN(prim_radcoefs[2].ka, prim_radcoefs[3].ka));
    const double prim_ka_max = MMAX
      (MMAX(prim_radcoefs[0].ka, prim_radcoefs[1].ka),
       MMAX(prim_radcoefs[2].ka, prim_radcoefs[3].ka));
    radcoefs[ATRSTM_RADCOEF_Ka] =
      prim_radcoefs[0].ka * args->bcoords[0]
    + prim_radcoefs[1].ka * args->bcoords[1]
    + prim_radcoefs[2].ka * args->bcoords[2]
    + prim_radcoefs[3].ka * args->bcoords[3];
    radcoefs[ATRSTM_RADCOEF_Ka] = /* Handle numerical uncertainty */
      CLAMP(radcoefs[ATRSTM_RADCOEF_Ka], prim_ka_min, prim_ka_max);
    ASSERT(radcoefs[ATRSTM_RADCOEF_Ka] >= args->k_min[ATRSTM_RADCOEF_Ka]);
    ASSERT(radcoefs[ATRSTM_RADCOEF_Ka] <= args->k_max[ATRSTM_RADCOEF_Ka]);
  }
  if(args->radcoefs_mask & ATRSTM_RADCOEF_FLAG_Ks) {
    const double prim_ks_min = MMIN
      (MMIN(prim_radcoefs[0].ks, prim_radcoefs[1].ks),
       MMIN(prim_radcoefs[2].ks, prim_radcoefs[3].ks));
    const double prim_ks_max = MMAX
      (MMAX(prim_radcoefs[0].ks, prim_radcoefs[1].ks),
       MMAX(prim_radcoefs[2].ks, prim_radcoefs[3].ks));
    radcoefs[ATRSTM_RADCOEF_Ks] =
      prim_radcoefs[0].ks * args->bcoords[0]
    + prim_radcoefs[1].ks * args->bcoords[1]
    + prim_radcoefs[2].ks * args->bcoords[2]
    + prim_radcoefs[3].ks * args->bcoords[3];
    radcoefs[ATRSTM_RADCOEF_Ks] = /* Handle numerical uncertainty */
      CLAMP(radcoefs[ATRSTM_RADCOEF_Ks], prim_ks_min, prim_ks_max);
    ASSERT(radcoefs[ATRSTM_RADCOEF_Ks] >= args->k_min[ATRSTM_RADCOEF_Ks]);
    ASSERT(radcoefs[ATRSTM_RADCOEF_Ks] <= args->k_max[ATRSTM_RADCOEF_Ks]);
  }
  if(args->radcoefs_mask & ATRSTM_RADCOEF_FLAG_Kext) {
    const double prim_kext_min = MMIN
      (MMIN(prim_radcoefs[0].kext, prim_radcoefs[1].kext),
       MMIN(prim_radcoefs[2].kext, prim_radcoefs[3].kext));
    const double prim_kext_max = MMAX
      (MMAX(prim_radcoefs[0].kext, prim_radcoefs[1].kext),
       MMAX(prim_radcoefs[2].kext, prim_radcoefs[3].kext));
    radcoefs[ATRSTM_RADCOEF_Kext] =
      prim_radcoefs[0].kext * args->bcoords[0]
    + prim_radcoefs[1].kext * args->bcoords[1]
    + prim_radcoefs[2].kext * args->bcoords[2]
    + prim_radcoefs[3].kext * args->bcoords[3];
    radcoefs[ATRSTM_RADCOEF_Kext] = /* Handle numerical uncertainty */
      CLAMP(radcoefs[ATRSTM_RADCOEF_Kext], prim_kext_min, prim_kext_max);
    ASSERT(radcoefs[ATRSTM_RADCOEF_Kext] >= args->k_min[ATRSTM_RADCOEF_Kext]);
    ASSERT(radcoefs[ATRSTM_RADCOEF_Kext] <= args->k_max[ATRSTM_RADCOEF_Kext]);
  }

exit:
  return res;
error:
  goto exit;
}

res_T
primitive_compute_radcoefs
  (const struct atrstm* atrstm,
   const struct atrri_refractive_index* refract_id,
   const struct suvm_primitive* prim,
   const int radcoefs_mask, /* Combination of atrstm_radcoef_flag */
   struct radcoefs radcoefs[])
{
  struct radcoefs_compute_args args = RADCOEFS_COMPUTE_ARGS_NULL;
  struct atrtp_desc desc = ATRTP_DESC_NULL;
  size_t inode;
  res_T res = RES_OK;
  ASSERT(atrstm && prim && prim->nvertices == 4 && refract_id && radcoefs);

  res = atrtp_get_desc(atrstm->atrtp, &desc);
  if(res != RES_OK) goto error;

  /* Setup the constant input arguments of the optical properties computation
   * routine */
  args.lambda = refract_id->wavelength;
  args.n = refract_id->n;
  args.kappa = refract_id->kappa;
  args.fractal_prefactor = atrstm->fractal_prefactor;
  args.fractal_dimension = atrstm->fractal_dimension;
  args.radcoefs_mask = radcoefs_mask;

  FOR_EACH(inode, 0, 4) {
    const double* node;

    /* Fetch the thermodynamic properties of the node */
    node = atrtp_desc_get_node_properties(&desc, prim->indices[inode]);

    /* Setup the per node input arguments of the optical properties computation
     * routine */
    args.soot_volumic_fraction =
      node[ATRTP_SOOT_VOLFRAC];
    args.soot_primary_particles_count =
      node[ATRTP_SOOT_PRIMARY_PARTICLES_COUNT];
    args.soot_primary_particles_diameter =
      node[ATRTP_SOOT_PRIMARY_PARTICLES_DIAMETER];

    /* Compute the node optical properties */
    radcoefs_compute(&radcoefs[inode], &args);
  }

exit:
  return res;
error:
  goto exit;
}

res_T
dump_radcoefs
  (const struct atrstm* atrstm,
   const struct atrri_refractive_index* refract_id,
   FILE* stream)
{
  struct radcoefs_compute_args args = RADCOEFS_COMPUTE_ARGS_NULL;
  struct atrtp_desc desc = ATRTP_DESC_NULL;
  struct radcoefs props_min;
  struct radcoefs props_max;
  size_t inode;

  res_T res = RES_OK;
  ASSERT(atrstm && stream);

  res = atrtp_get_desc(atrstm->atrtp, &desc);
  if(res != RES_OK) goto error;

  /* Setup the constant input params of the optical properties computation */
  args.lambda = refract_id->wavelength;
  args.n = refract_id->n;
  args.kappa = refract_id->kappa;
  args.fractal_prefactor = atrstm->fractal_prefactor;
  args.fractal_dimension = atrstm->fractal_dimension;
  args.radcoefs_mask = ATRSTM_RADCOEFS_MASK_ALL;

  props_min.ka = DBL_MAX;
  props_max.ka =-DBL_MAX;
  props_min.ks = DBL_MAX;
  props_max.ks =-DBL_MAX;
  props_min.kext = DBL_MAX;
  props_max.kext =-DBL_MAX;

  #define FPRINTF(Text, Args) {                                                \
    const int n = fprintf(stream, Text COMMA_##Args LIST_##Args);              \
    if(n < 0) {                                                                \
      log_err(atrstm, "Error writing optical properties.\n");                  \
      res = RES_IO_ERR;                                                        \
      goto error;                                                              \
    }                                                                          \
  } (void)0

  FPRINTF("# Ka Ks Kext\n", ARG0());

  FOR_EACH(inode, 0, desc.nnodes) {
    struct radcoefs props;
    const double* node;

    /* Fetch the thermodynamic properties of the node */
    node = atrtp_desc_get_node_properties(&desc, inode);

    /* Setup the per node input args of the optical properties computation */
    args.soot_volumic_fraction =
      node[ATRTP_SOOT_VOLFRAC];
    args.soot_primary_particles_count =
      node[ATRTP_SOOT_PRIMARY_PARTICLES_COUNT];
    args.soot_primary_particles_diameter =
      node[ATRTP_SOOT_PRIMARY_PARTICLES_DIAMETER];

    /* Compute the node optical properties */
    radcoefs_compute(&props, &args);

    FPRINTF("%g %g %g\n", ARG3(props.ka, props.ks, props.kext));

    props_min.ka = MMIN(props_min.ka, props.ka);
    props_max.ka = MMAX(props_max.ka, props.ka);
    props_min.ks = MMIN(props_min.ks, props.ks);
    props_max.ks = MMAX(props_max.ks, props.ks);
    props_min.kext = MMIN(props_min.kext, props.kext);
    props_max.kext = MMAX(props_max.kext, props.kext);

  }

  FPRINTF("# Bounds = [%g %g %g], [%g, %g, %g]\n", ARG6
    (props_min.ka, props_min.ks, props_min.kext,
     props_max.ka, props_max.ks, props_max.kext));

  #undef FPRINTF

exit:
  return res;
error:
  goto exit;
}

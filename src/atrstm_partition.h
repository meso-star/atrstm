/* Copyright (C) 2022, 2023 |Méso|Star> (contact@meso-star.com)
 * Copyright (C) 2020, 2021 Centre National de la Recherche Scientifique
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef ATRSTM_PARTITION_H
#define ATRSTM_PARTITION_H

#include "atrstm_svx.h"
#include <rsys/list.h>
#include <string.h>

/* Definition of a partition along the 3 axis */
#define LOG2_PARTITION_DEFINITION 5
#define PARTITION_DEFINITION BIT(LOG2_PARTITION_DEFINITION) /*32*/
#define PARTITION_NVOXELS                                                      \
  ( PARTITION_DEFINITION                                                       \
  * PARTITION_DEFINITION                                                       \
  * PARTITION_DEFINITION)

/* Forward declarations */
struct cond;
struct mem_allocator;
struct mutex;

struct part {
  /* Morton ordered list of voxels */
  float voxels[PARTITION_NVOXELS * NFLOATS_PER_VOXEL];
  struct list_node node;
  size_t id; /* Unique identifier of the partition */
};

struct pool {
  /* Linked list of pre-allocated partitions */
  struct list_node parts_free;

  /* List of available partition sorted in ascending order wrt partition id */
  struct list_node parts_full;

  struct mutex* mutex;
  struct cond* cond_new;
  struct cond* cond_fetch;

  size_t next_part_id; /* Indentifier of the next partition */

  struct mem_allocator* allocator;

  ATOMIC error;
};

static INLINE void
part_init(struct part* part)
{
  ASSERT(part);
  list_init(&part->node);
  part->id = SIZE_MAX;
}

static INLINE void
part_release(struct part* part)
{
  ASSERT(part && is_list_empty(&part->node));
  (void)part; /* Do nothing */
}

static FINLINE float*
part_get_voxel
  (struct part* part,
   const size_t ivoxel) /* Morton code of the voxel */
{
  ASSERT(part);
  ASSERT(ivoxel < PARTITION_NVOXELS);
  return part->voxels + ivoxel * NFLOATS_PER_VOXEL;
}

static FINLINE void
part_clear_voxels(struct part* part)
{
  size_t ivox;
  FOR_EACH(ivox, 0, PARTITION_NVOXELS) {
    vox_clear(part_get_voxel(part, ivox));
  }
}

extern LOCAL_SYM res_T
pool_init
  (struct mem_allocator* allocator, /* May be NULL <=> default allocator */
   const size_t npartitions, /* #partitions managed by the pool */
   struct pool* pool);

extern LOCAL_SYM void
pool_release
  (struct pool* pool);

/* Return a free partition. Wait until a free partition is available. Return
 * NULL on error. */
extern LOCAL_SYM struct part*
pool_next_partition
  (struct pool* pool);

/* Register the partition as a free partition against the pool */
extern LOCAL_SYM void
pool_free_partition
  (struct pool* pool,
   struct part* part);

/* Register the partition as a valid partition that can be fetched */
extern LOCAL_SYM void
pool_commit_partition
  (struct pool* pool,
   struct part* part);

/* Return the partition whose id is `part_id`. Wait until the expected
 * partition is available. Return NULL on error. */
extern LOCAL_SYM struct part*
pool_fetch_partition
  (struct pool* pool,
   const size_t part_id);

extern LOCAL_SYM void
pool_invalidate
  (struct pool* pool);

#endif /* ATRSTM_PARTITION_H */

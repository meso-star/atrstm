/* Copyright (C) 2022, 2023 |Méso|Star> (contact@meso-star.com)
 * Copyright (C) 2020, 2021 Centre National de la Recherche Scientifique
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "atrstm_c.h"
#include "atrstm_log.h"
#include "atrstm_radcoefs.h"
#include "atrstm_radcoefs_simd4.h"

#include <astoria/atrtp.h>
#include <star/suvm.h>

#include <rsys/cstr.h>

/*******************************************************************************
 * Local functions
 ******************************************************************************/
res_T
fetch_radcoefs_simd4
  (const struct atrstm* atrstm,
   const struct atrstm_fetch_radcoefs_args* args,
   atrstm_radcoefs_T radcoefs)
{
  struct radcoefs_simd4 prim_radcoefs;
  v4f_T bcoords;
  res_T res = RES_OK;

  if(!atrstm
  || !check_fetch_radcoefs_args(atrstm, args, FUNC_NAME)
  || !radcoefs) {
    res = RES_BAD_ARG;
    goto error;
  }
  memset(radcoefs, 0, sizeof(double[ATRSTM_RADCOEFS_COUNT__]));

  /* Compute the radiative properties of the primitive nodes */
  res = primitive_compute_radcoefs_simd4(atrstm, &atrstm->refract_id,
    &args->prim, args->radcoefs_mask, &prim_radcoefs);
  if(res != RES_OK) {
    log_err(atrstm,
      "Error computing the radiative properties for the primitive %lu -- %s.\n",
      (unsigned long)args->prim.iprim,
      res_to_cstr(res));
    goto error;
  }

  bcoords = v4f_set
    ((float)args->bcoords[0],
     (float)args->bcoords[1],
     (float)args->bcoords[2],
     (float)args->bcoords[3]);

  /* Linearly interpolate the node radiative properties */
  if(args->radcoefs_mask & ATRSTM_RADCOEF_FLAG_Ka) {
    const float prim_ka_min = v4f_x(v4f_reduce_min(prim_radcoefs.ka));
    const float prim_ka_max = v4f_x(v4f_reduce_max(prim_radcoefs.ka));
    radcoefs[ATRSTM_RADCOEF_Ka] = v4f_x(v4f_dot(prim_radcoefs.ka, bcoords));
    radcoefs[ATRSTM_RADCOEF_Ka] = /* Handle numerical uncertainty */
      CLAMP(radcoefs[ATRSTM_RADCOEF_Ka], prim_ka_min, prim_ka_max);
    ASSERT(radcoefs[ATRSTM_RADCOEF_Ka] >= args->k_min[ATRSTM_RADCOEF_Ka]);
    ASSERT(radcoefs[ATRSTM_RADCOEF_Ka] <= args->k_max[ATRSTM_RADCOEF_Ka]);
  }
  if(args->radcoefs_mask & ATRSTM_RADCOEF_FLAG_Ks) {
    const float prim_ks_min = v4f_x(v4f_reduce_min(prim_radcoefs.ks));
    const float prim_ks_max = v4f_x(v4f_reduce_max(prim_radcoefs.ks));
    radcoefs[ATRSTM_RADCOEF_Ks] = v4f_x(v4f_dot(prim_radcoefs.ks, bcoords));
    radcoefs[ATRSTM_RADCOEF_Ks] = /* Handle numerical uncertainty */
      CLAMP(radcoefs[ATRSTM_RADCOEF_Ks], prim_ks_min, prim_ks_max);
    ASSERT(radcoefs[ATRSTM_RADCOEF_Ks] >= args->k_min[ATRSTM_RADCOEF_Ks]);
    ASSERT(radcoefs[ATRSTM_RADCOEF_Ks] <= args->k_max[ATRSTM_RADCOEF_Ks]);
  }
  if(args->radcoefs_mask & ATRSTM_RADCOEF_FLAG_Kext) {
    const float prim_kext_min = v4f_x(v4f_reduce_min(prim_radcoefs.kext));
    const float prim_kext_max = v4f_x(v4f_reduce_max(prim_radcoefs.kext));
    radcoefs[ATRSTM_RADCOEF_Kext] = v4f_x(v4f_dot(prim_radcoefs.kext, bcoords));
    radcoefs[ATRSTM_RADCOEF_Kext] = /* Handle numerical uncertainty */
      CLAMP(radcoefs[ATRSTM_RADCOEF_Kext], prim_kext_min, prim_kext_max);
    ASSERT(radcoefs[ATRSTM_RADCOEF_Kext] >= args->k_min[ATRSTM_RADCOEF_Kext]);
    ASSERT(radcoefs[ATRSTM_RADCOEF_Kext] <= args->k_max[ATRSTM_RADCOEF_Kext]);
  }

exit:
  return res;
error:
  goto exit;
}

res_T
primitive_compute_radcoefs_simd4
  (const struct atrstm* atrstm,
   const struct atrri_refractive_index* refract_id,
   const struct suvm_primitive* prim,
   const int radcoefs_mask, /* Combination of atrstm_radcoef_flag */
   struct radcoefs_simd4* radcoefs)
{
  struct radcoefs_compute_simd4_args args = RADCOEFS_COMPUTE_SIMD4_ARGS_NULL;
  struct atrtp_desc desc = ATRTP_DESC_NULL;
  const double* node[4];
  res_T res = RES_OK;
  ASSERT(atrstm && prim && prim->nvertices == 4 && refract_id && radcoefs);

  res = atrtp_get_desc(atrstm->atrtp, &desc);
  if(res != RES_OK) goto error;

  /* Setup the constant input arguments of the optical properties computation
   * routine */
  args.lambda = v4f_set1((float)refract_id->wavelength);
  args.n = v4f_set1((float)refract_id->n);
  args.kappa = v4f_set1((float)refract_id->kappa);
  args.fractal_prefactor = v4f_set1((float)atrstm->fractal_prefactor);
  args.fractal_dimension = v4f_set1((float)atrstm->fractal_dimension);
  args.radcoefs_mask = radcoefs_mask;

  /* Fetch the thermodynamic properties of the 4 primitive nodes */
  node[0] = atrtp_desc_get_node_properties(&desc, prim->indices[0]);
  node[1] = atrtp_desc_get_node_properties(&desc, prim->indices[1]);
  node[2] = atrtp_desc_get_node_properties(&desc, prim->indices[2]);
  node[3] = atrtp_desc_get_node_properties(&desc, prim->indices[3]);

  /* Scatter Soot properties */
  args.soot_volumic_fraction = v4f_set
    ((float)node[0][ATRTP_SOOT_VOLFRAC],
     (float)node[1][ATRTP_SOOT_VOLFRAC],
     (float)node[2][ATRTP_SOOT_VOLFRAC],
     (float)node[3][ATRTP_SOOT_VOLFRAC]);
  args.soot_primary_particles_count = v4f_set
    ((float)node[0][ATRTP_SOOT_PRIMARY_PARTICLES_COUNT],
     (float)node[1][ATRTP_SOOT_PRIMARY_PARTICLES_COUNT],
     (float)node[2][ATRTP_SOOT_PRIMARY_PARTICLES_COUNT],
     (float)node[3][ATRTP_SOOT_PRIMARY_PARTICLES_COUNT]);
  args.soot_primary_particles_diameter = v4f_set
    ((float)node[0][ATRTP_SOOT_PRIMARY_PARTICLES_DIAMETER],
     (float)node[1][ATRTP_SOOT_PRIMARY_PARTICLES_DIAMETER],
     (float)node[2][ATRTP_SOOT_PRIMARY_PARTICLES_DIAMETER],
     (float)node[3][ATRTP_SOOT_PRIMARY_PARTICLES_DIAMETER]);

  /* Compute the per primitive node optical properties */
  radcoefs_compute_simd4(radcoefs, &args);

exit:
  return res;
error:
  goto exit;
}

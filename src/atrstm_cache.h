/* Copyright (C) 2022, 2023 |Méso|Star> (contact@meso-star.com)
 * Copyright (C) 2020, 2021 Centre National de la Recherche Scientifique
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef ATRSTM_CACHE_H
#define ATRSTM_CACHE_H

#include <rsys/rsys.h>
#include <stdio.h>

/* Cache of the acceleration data structures used to partition the optical
 * properties of the semi transparent medium */
struct cache;

extern LOCAL_SYM res_T
cache_create
  (struct atrstm* atrstm,
   const char* cache_name,
   struct cache** cache);

extern LOCAL_SYM void
cache_ref_get
  (struct cache* cache);

extern LOCAL_SYM void
cache_ref_put
  (struct cache* cache);

extern LOCAL_SYM FILE*
cache_get_stream
  (struct cache* cache);

extern LOCAL_SYM int
cache_is_empty
  (const struct cache* cache);

extern LOCAL_SYM const char*
cache_get_name
  (const struct cache* cache);

#endif /* ATRSTM_CACHE_H */

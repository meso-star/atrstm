/* Copyright (C) 2022, 2023 |Méso|Star> (contact@meso-star.com)
 * Copyright (C) 2020, 2021 Centre National de la Recherche Scientifique
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef ATRSTM_RADCOEFS_H
#define ATRSTM_RADCOEFS_H

#include "atrstm.h"
#include "atrstm_rdgfa.h"

#include <rsys/math.h>
#include <rsys/rsys.h>

/* Radiative coefficients in m^-1 */
struct radcoefs {
  double ka; /* Absorption coefficient */
  double ks; /* Scattering coefficient */
  double kext; /* Extinction coefficient */
};
#define RADCOEFS_NULL__ {0,0,0}
static const struct radcoefs RADCOEFS_NULL = RADCOEFS_NULL__;

struct radcoefs_compute_args {
  double lambda; /* wavelength [nm] */
  double n; /* Refractive index (real component) */
  double kappa; /* Refractive index (imaginary component) */
  double fractal_prefactor;
  double fractal_dimension;
  double soot_volumic_fraction; /* In m^3(soot) / m^3 */
  double soot_primary_particles_count;
  double soot_primary_particles_diameter; /* In nm */
  int radcoefs_mask; /* Combination of atrstm_radcoef_flag */
};
#define RADCOEFS_COMPUTE_ARGS_NULL__ {0,0,0,0,0,0,0,0,0}
static const struct radcoefs_compute_args RADCOEFS_COMPUTE_ARGS_NULL =
  RADCOEFS_COMPUTE_ARGS_NULL__;

/* Forward declarations */
struct atrri_refractive_index;
struct atrstm;
struct suvm_primitive;

extern LOCAL_SYM int
check_fetch_radcoefs_args
  (const struct atrstm* atrstm,
   const struct atrstm_fetch_radcoefs_args* args,
   const char* func_name);

extern LOCAL_SYM res_T
fetch_radcoefs
  (const struct atrstm* atrstm,
   const struct atrstm_fetch_radcoefs_args* args,
   atrstm_radcoefs_T radcoefs);

/* Write per node radiative coefficients regarding the submitted refracted
 * index */
extern LOCAL_SYM res_T
dump_radcoefs
  (const struct atrstm* atrstm,
   const struct atrri_refractive_index* refract_id,
   FILE* stream);

extern LOCAL_SYM res_T
primitive_compute_radcoefs
  (const struct atrstm* atrstm,
   const struct atrri_refractive_index* refract_id,
   const struct suvm_primitive* prim,
   const int radcoefs_mask,
   struct radcoefs radcoefs[]); /* Per primitive node radiative coefficients */

static INLINE res_T
primitive_compute_radcoefs_range
  (const struct atrstm* atrstm,
   const struct atrri_refractive_index* refract_id,
   const struct suvm_primitive* prim,
   struct radcoefs* radcoefs_min,
   struct radcoefs* radcoefs_max)
{
  struct radcoefs props[4];
  res_T res = RES_OK;
  ASSERT(radcoefs_min && radcoefs_max);

  res = primitive_compute_radcoefs
    (atrstm, refract_id, prim, ATRSTM_RADCOEFS_MASK_ALL, props);
  if(res != RES_OK) return res;

  radcoefs_min->ka = MMIN
    (MMIN(props[0].ka, props[1].ka),
     MMIN(props[2].ka, props[3].ka));
  radcoefs_min->ks = MMIN
    (MMIN(props[0].ks, props[1].ks),
     MMIN(props[2].ks, props[3].ks));
  radcoefs_min->kext = MMIN
    (MMIN(props[0].kext, props[1].kext),
     MMIN(props[2].kext, props[3].kext));

  radcoefs_max->ka = MMAX
    (MMAX(props[0].ka, props[1].ka),
     MMAX(props[2].ka, props[3].ka));
  radcoefs_max->ks = MMAX
    (MMAX(props[0].ks, props[1].ks),
     MMAX(props[2].ks, props[3].ks));
  radcoefs_max->kext = MMAX
    (MMAX(props[0].kext, props[1].kext),
     MMAX(props[2].kext, props[3].kext));

  return RES_OK;
}

/* Compute the radiative coefficients of the semi transparent medium as described
 * in "Effects of multiple scattering on radiative properties of soot fractal
 * aggregates" J. Yon et al., Journal of Quantitative Spectroscopy and
 * Radiative Transfer Vol 133, pp. 374-381, 2014 */
static INLINE void
radcoefs_compute
  (struct radcoefs* radcoefs,
   const struct radcoefs_compute_args* args)
{
  /* Input variables */
  const double lambda = args->lambda; /* [nm] */
  const double n = args->n;
  const double kappa = args->kappa;
  const double Fv = args->soot_volumic_fraction; /* [nm^3(soot)/nm] */
  const double Np = args->soot_primary_particles_count;
  const double Dp = args->soot_primary_particles_diameter; /* [nm] */
  const double kf = args->fractal_prefactor;
  const double Df = args->fractal_dimension;
  const int ka = (args->radcoefs_mask & ATRSTM_RADCOEF_FLAG_Ka) != 0;
  const int ks = (args->radcoefs_mask & ATRSTM_RADCOEF_FLAG_Ks) != 0;
  const int kext = (args->radcoefs_mask & ATRSTM_RADCOEF_FLAG_Kext) != 0;

  /* Clean up radiative coefficients */
  radcoefs->ka = 0;
  radcoefs->ks = 0;
  radcoefs->kext = 0;

  if(Np != 0 && Fv != 0 && (args->radcoefs_mask & ATRSTM_RADCOEFS_MASK_ALL)) {
    /* Precomputed values */
    const double n2 = n*n;
    const double kappa2 = kappa*kappa;
    const double four_n2_kappa2 = 4*n2*kappa2;
    const double xp = (PI*Dp) / lambda;
    const double xp3 = xp*xp*xp;
    const double k = (2*PI) / lambda;
    const double k2 = k*k;

    const double Va = (Np*PI*Dp*Dp*Dp) / 6; /* [nm^3] */
    const double rho = Fv / Va; /* [#aggregates / nm^3] */
    const double tmp0 = 1.e9 * rho;

    /* E(m), m = n + i*kappa */
    const double tmp1 = (n2 - kappa2 + 2);
    const double denom = tmp1*tmp1 + four_n2_kappa2;
    const double Em = (6*n*kappa) / denom;

    if(ka || kext) {
      /* Absorption cross section, [nm^3/aggrefate] */
      const double Cabs = Np * (4*PI*xp3)/(k2) * Em;

      /* Absorption coefficient, [m^-1] */
      radcoefs->ka = tmp0 * Cabs;
    }

    if(ks || kext) {
      /* F(m), m = n + i*kappa */
      const double tmp2 = ((n2-kappa2 - 1)*(n2-kappa2+2) + four_n2_kappa2)/denom;
      const double Fm = tmp2*tmp2 + Em*Em;

      /* Gyration radius */
      const double Rg = compute_gyration_radius(kf, Df, Np, Dp); /* [nm] */
      const double Rg2 = Rg*Rg;
      const double g = pow(1 + 4*k2*Rg2/(3*Df), -Df*0.5);

      /* Scattering cross section, [nm^3/aggrefate] */
      const double Csca = Np*Np * (8*PI*xp3*xp3) / (3*k2) * Fm * g;

      /* Scattering coefficient, [m^-1] */
      radcoefs->ks = tmp0 * Csca;
    }

    if(kext) {
      radcoefs->kext = radcoefs->ka + radcoefs->ks;
    }
    ASSERT(denom != 0 && Df != 0 && Dp != 0);
  }
}

#endif /* ATRSTM_RADCOEFS_H */

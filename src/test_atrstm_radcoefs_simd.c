/* Copyright (C) 2022, 2023 |Méso|Star> (contact@meso-star.com)
 * Copyright (C) 2020, 2021 Centre National de la Recherche Scientifique
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "atrstm_radcoefs_simd4.h"

int
main(int argc, char** argv)
{
  const double ka_ref = 5.7382401729092799E-1;
  const double ks_ref = 7.2169062018378995E-6;

  const double ka_ref2[4] = {
    0.52178067472799794,
    0.52178067472799794,
    1.0435613494559959,
    0.52178067472799794
  };
  const double ks_ref2[4] = {
    9.6010140939975883e-002,
    3.3272961678492224e-002,
    0.19202028187995177,
    9.9964602374815484e-002
  };
  struct radcoefs_simd4 radcoefs = RADCOEFS_SIMD4_NULL;
  struct radcoefs_compute_simd4_args args = RADCOEFS_COMPUTE_SIMD4_ARGS_NULL;
  float ALIGN(16) ka[4], ks[4], kext[4];
  (void)argc, (void)argv;

  args.lambda = v4f_set1(633.f);
  args.n = v4f_set1(1.90f);
  args.kappa = v4f_set1(0.55f);
  args.fractal_prefactor = v4f_set1(1.70f);
  args.fractal_dimension = v4f_set1(1.75f);
  args.soot_volumic_fraction = v4f_set(1e-7f, 0.f, 1e-7f, 1e-7f);
  args.soot_primary_particles_count = v4f_set(100.f, 100.f, 0.f, 100.f);
  args.soot_primary_particles_diameter = v4f_set1(1.f);
  args.radcoefs_mask = ATRSTM_RADCOEFS_MASK_ALL;

  radcoefs_compute_simd4(&radcoefs, &args);

  v4f_store(ka, radcoefs.ka);
  v4f_store(ks, radcoefs.ks);
  v4f_store(kext, radcoefs.kext);
  printf("ka = {%g, %g, %g, %g}; ks = {%g, %g, %g, %g}\n",
    SPLIT4(ka), SPLIT4(ks));

  CHK(eq_eps(ka[0], ka_ref, ka_ref*1.e-6));
  CHK(eq_eps(ka[3], ka_ref, ka_ref*1.e-6));
  CHK(ka[1] == 0);
  CHK(ka[2] == 0);

  CHK(eq_eps(ks[0], ks_ref, ks_ref*1.e-6));
  CHK(eq_eps(ks[3], ks_ref, ks_ref*1.e-6));
  CHK(ks[1] == 0);
  CHK(ks[2] == 0);

  CHK(eq_eps(kext[0], ka_ref+ks_ref, (ka_ref+ks_ref)*1e-6));
  CHK(eq_eps(kext[3], ka_ref+ks_ref, (ka_ref+ks_ref)*1e-6));
  CHK(kext[1] == 0);
  CHK(kext[2] == 0);

  args.radcoefs_mask = ATRSTM_RADCOEF_FLAG_Ka;
  radcoefs_compute_simd4(&radcoefs, &args);
  v4f_store(ka, radcoefs.ka);
  v4f_store(ks, radcoefs.ks);
  v4f_store(kext, radcoefs.kext);
  CHK(eq_eps(ka[0], ka_ref, ka_ref*1.e-6));
  CHK(eq_eps(ka[3], ka_ref, ka_ref*1.e-6));
  CHK(ka[1] == 0);
  CHK(ka[2] == 0);
  CHK(ks[0] == 0 && ks[1] == 0 && ks[2] == 0 && ks[0] == 0);
  CHK(kext[0] == 0 && kext[1] == 0 && kext[2] == 0 && kext[0] == 0);

  args.radcoefs_mask = ATRSTM_RADCOEF_FLAG_Ks;
  radcoefs_compute_simd4(&radcoefs, &args);
  v4f_store(ka, radcoefs.ka);
  v4f_store(ks, radcoefs.ks);
  v4f_store(kext, radcoefs.kext);
  CHK(eq_eps(ks[0], ks_ref, ks_ref*1.e-6));
  CHK(eq_eps(ks[3], ks_ref, ks_ref*1.e-6));
  CHK(ks[1] == 0);
  CHK(ks[2] == 0);
  CHK(ka[0] == 0 && ka[1] == 0 && ka[2] == 0 && ka[0] == 0);
  CHK(kext[0] == 0 && kext[1] == 0 && kext[2] == 0 && kext[0] == 0);

  /* Note that actually even though Ka and Ks are note required they are
   * internally computed to evaluate kext and are returned to the caller. Their
   * value are thus not null */
  args.radcoefs_mask = ATRSTM_RADCOEF_FLAG_Kext;
  radcoefs_compute_simd4(&radcoefs, &args);
  v4f_store(kext, radcoefs.kext);
  CHK(eq_eps(kext[0], ka_ref+ks_ref, (ka_ref+ks_ref)*1e-6));
  CHK(eq_eps(kext[3], ka_ref+ks_ref, (ka_ref+ks_ref)*1e-6));
  CHK(kext[1] == 0);
  CHK(kext[2] == 0);

  args.lambda = v4f_set1(633.f);
  args.n = v4f_set1(1.75f);
  args.kappa = v4f_set1(0.435f);
  args.fractal_prefactor = v4f_set1(1.70f);
  args.fractal_dimension = v4f_set1(1.75f);
  args.soot_volumic_fraction = v4f_set
    (9.9999999999999995e-008f,
     9.9999999999999995e-008f,
     1.9999999999999999e-007f,
     9.9999999999999995e-008f);
  args.soot_primary_particles_count = v4f_set
    (400.f,
     400.f,
     400.f,
     800.f);
  args.soot_primary_particles_diameter = v4f_set
    (34.000000006413003f,
     17.000000003206502f,
     34.000000006413003f,
     34.000000006413003f);
  args.radcoefs_mask = ATRSTM_RADCOEFS_MASK_ALL;
  radcoefs_compute_simd4(&radcoefs, &args);
  v4f_store(ka, radcoefs.ka);
  v4f_store(ks, radcoefs.ks);
  v4f_store(kext, radcoefs.kext);
  printf("ka = {%g, %g, %g, %g}; ks = {%g, %g, %g, %g}\n",
    SPLIT4(ka), SPLIT4(ks));
  CHK(eq_eps(ka[0], ka_ref2[0], ka_ref2[0]*1.e-6));
  CHK(eq_eps(ka[1], ka_ref2[1], ka_ref2[1]*1.e-6));
  CHK(eq_eps(ka[2], ka_ref2[2], ka_ref2[2]*1.e-6));
  CHK(eq_eps(ka[3], ka_ref2[3], ka_ref2[3]*1.e-6));
  CHK(eq_eps(ks[0], ks_ref2[0], ks_ref2[0]*1.e-6));
  CHK(eq_eps(ks[1], ks_ref2[1], ks_ref2[1]*1.e-6));
  CHK(eq_eps(ks[2], ks_ref2[2], ks_ref2[2]*1.e-6));
  CHK(eq_eps(ks[3], ks_ref2[3], ks_ref2[3]*1.e-6));
  CHK(eq_eps(kext[0], ka_ref2[0]+ks_ref2[0], (ka_ref2[0]+ks_ref2[0])*1.e-6));
  CHK(eq_eps(kext[1], ka_ref2[1]+ks_ref2[1], (ka_ref2[1]+ks_ref2[1])*1.e-6));
  CHK(eq_eps(kext[2], ka_ref2[2]+ks_ref2[2], (ka_ref2[2]+ks_ref2[2])*1.e-6));
  CHK(eq_eps(kext[3], ka_ref2[3]+ks_ref2[3], (ka_ref2[3]+ks_ref2[3])*1.e-6));

  return 0;
}


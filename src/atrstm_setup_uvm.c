/* Copyright (C) 2022, 2023 |Méso|Star> (contact@meso-star.com)
 * Copyright (C) 2020, 2021 Centre National de la Recherche Scientifique
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "atrstm_c.h"
#include "atrstm_log.h"

#include <star/smsh.h>
#include <star/suvm.h>

#include <rsys/clock_time.h>

/*******************************************************************************
 * Helper functions
 ******************************************************************************/
static res_T
check_smsh_desc(struct atrstm* atrstm, const struct smsh_desc* desc)
{
  res_T res = RES_OK;
  ASSERT(atrstm && desc);

  if(desc->dnode != 3 || desc->dcell != 4) {
    log_err(atrstm,
      "The mesh of a semi-transparent material must be a 3D tetrahedral mesh "
      "(dimension of the mesh: %u; dimension of the vertices: %u)\n",
      desc->dnode, desc->dcell);
    res = RES_BAD_ARG;
    goto error;
  }

exit:
  return res;
error:
  goto exit;
}

static INLINE void
tetrahedron_get_indices(const size_t itetra, size_t ids[4], void* ctx)
{
  const struct smsh_desc* smsh_desc = ctx;
  const uint64_t* indices = NULL;
  ASSERT(ctx && ids && itetra < smsh_desc->ncells && smsh_desc->dcell == 4);
  indices = smsh_desc_get_cell(smsh_desc, itetra);
  ids[0] = (size_t)indices[0];
  ids[1] = (size_t)indices[1];
  ids[2] = (size_t)indices[2];
  ids[3] = (size_t)indices[3];
}

static INLINE void
vertex_get_position(const size_t ivert, double pos[3], void* ctx)
{
  struct smsh_desc* smsh_desc = ctx;
  const double* position = NULL;
  ASSERT(ctx && pos && ivert < smsh_desc->nnodes && smsh_desc->dnode == 3);
  position = smsh_desc_get_node(smsh_desc, ivert);
  pos[0] = position[0];
  pos[1] = position[1];
  pos[2] = position[2];
}

/*******************************************************************************
 * Local functions
 ******************************************************************************/
res_T
setup_unstructured_volumetric_mesh
  (struct atrstm* atrstm,
   const int precompute_normals,
   const char* smsh_filename,
   struct suvm_volume** out_volume)
{
  struct suvm_tetrahedral_mesh_args mesh_args = SUVM_TETRAHEDRAL_MESH_ARGS_NULL;
  struct smsh_create_args smsh_create_args = SMSH_CREATE_ARGS_DEFAULT;
  struct smsh_desc smsh_desc = SMSH_DESC_NULL;
  struct smsh_load_args smsh_load_args = SMSH_LOAD_ARGS_NULL;
  struct smsh* smsh = NULL;
  struct suvm_volume* volume = NULL;
  struct time t0, t1;
  char buf[128];
  res_T res = RES_OK;
  ASSERT(atrstm && smsh_filename && out_volume);

  log_info(atrstm, "Load and structure the volumetric mesh '%s'.\n",
    smsh_filename);

  time_current(&t0);

  /* Load the volumetric mesh */
  smsh_create_args.logger = atrstm->logger;
  smsh_create_args.allocator = atrstm->allocator;
  smsh_create_args.verbose = atrstm->verbose;
  res = smsh_create(&smsh_create_args, &smsh);
  if(res != RES_OK) goto error;
  smsh_load_args.path = smsh_filename;
  smsh_load_args.memory_mapping = 1;
  res = smsh_load(smsh, &smsh_load_args);
  if(res != RES_OK) goto error;
  res = smsh_get_desc(smsh, &smsh_desc);
  if(res != RES_OK) goto error;
  res = check_smsh_desc(atrstm, &smsh_desc);
  if(res != RES_OK) goto error;

  /* Partition the unstructured volumetric mesh */
  mesh_args.ntetrahedra = smsh_desc.ncells;
  mesh_args.nvertices = smsh_desc.nnodes;
  mesh_args.get_indices = tetrahedron_get_indices;
  mesh_args.get_position = vertex_get_position;
  mesh_args.tetrahedron_data = SUVM_DATA_NULL; /* Tetra data are not in SUVM */
  mesh_args.vertex_data = SUVM_DATA_NULL; /* Vertex data are not in SUVM */
  mesh_args.precompute_normals = precompute_normals;
  mesh_args.context = &smsh_desc;
  res = suvm_tetrahedral_mesh_create(atrstm->suvm, &mesh_args, &volume);
  if(res != RES_OK) goto error;

  /* Report time */
  time_sub(&t0, time_current(&t1), &t0);
  time_dump(&t0, TIME_ALL, NULL, buf, sizeof(buf));
  log_info(atrstm, "Setup volumetric mesh in %s.\n", buf);

exit:
  *out_volume = volume;
  if(smsh) SMSH(ref_put(smsh));
  return res;
error:
  if(volume) {
    SUVM(volume_ref_put(volume));
    volume = NULL;
  }
  goto exit;
}

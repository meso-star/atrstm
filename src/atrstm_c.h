/* Copyright (C) 2022, 2023 |Méso|Star> (contact@meso-star.com)
 * Copyright (C) 2020, 2021 Centre National de la Recherche Scientifique
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef ATRSTM_C_H
#define ATRSTM_C_H

#include "atrstm.h"

#include <astoria/atrri.h>

#include <rsys/logger.h>
#include <rsys/mem_allocator.h>
#include <rsys/ref_count.h>
#include <rsys/str.h>

/* Forward declaration of external data types */
struct atrri;
struct atrtp;
struct cache;
struct suvm;

struct atrstm {
  struct atrtp* atrtp; /* Handle toward the Astoria Thermodynamic Properties */
  struct atrri* atrri; /* Handle toward the Astoria Refractive Indices */

  /* Unstructured volumetric mesh */
  struct suvm_device* suvm;
  struct suvm_volume* volume;

  struct svx_device* svx;
  struct svx_tree* octree;

  unsigned nthreads; /* #nthreads */

  struct str name; /* Name of the semi-transparent medium */

  double fractal_prefactor;
  double fractal_dimension;

  enum atrstm_spectral_type spectral_type;
  double wlen_range[2]; /* Spectral range in nm */
  unsigned grid_max_definition[3];
  double optical_thickness;

  /* Pre-fetched refractive id in shortwave */
  struct atrri_refractive_index refract_id;

  struct cache* cache; /* Cache of the SVX data structures */

  struct mem_allocator* allocator;
  struct mem_allocator svx_allocator;
  struct logger* logger;
  struct logger logger__; /* Default logger */
  int verbose;
  int use_simd;
  ref_T ref;
};

extern LOCAL_SYM res_T
setup_unstructured_volumetric_mesh
  (struct atrstm* atrstm,
   const int precompute_normals,
   const char* sth_filename,
   struct suvm_volume** out_volume);

extern LOCAL_SYM void
dump_memory_size
  (const size_t size, /* In Bytes */
   size_t* real_dump_len, /* May be NULL */
   char* dump, /* May be NULL */
   size_t max_dump_len);

#endif /* ATRSTM_C_H */

/* Copyright (C) 2022, 2023 |Méso|Star> (contact@meso-star.com)
 * Copyright (C) 2020, 2021 Centre National de la Recherche Scientifique
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "atrstm.h"

#include <rsys/cstr.h>
#include <rsys/mem_allocator.h>
#include <rsys/rsys.h>

#include <getopt.h>
#include <stdio.h>
#include <string.h>

struct args {
  struct atrstm_args atrstm;
  const char* dump_octree_filename; /* NULL <=> no dump */
  int quit;
};

#define ARGS_DEFAULT__ { ATRSTM_ARGS_DEFAULT__, NULL, 0 }
static const struct args ARGS_DEFAULT = ARGS_DEFAULT__;

/*******************************************************************************
 * Helper functions
 ******************************************************************************/
static void
print_help(const char* cmd)
{
  ASSERT(cmd);
  printf(
"Usage: %s <option>...\n"
"Test the Astoria: Semi-Transparent Medium library.\n",
    cmd);
  printf("\n");
  printf(
"  -d OCTREE      dump SVX octree to the OCTREE file.\n");
  printf(
"  -f FRACTAL_DIM fractal dimension. Its default value is %g.\n",
    ATRSTM_ARGS_DEFAULT.fractal_dimension);
  printf(
"  -g PREFACTOR   fractal prefactor. Its default value is %g.\n",
    ATRSTM_ARGS_DEFAULT.fractal_prefactor);
  printf(
"  -h             display this help and exit.\n");
  printf(
"  -m TETRAHEDRA  path toward the volumetric mesh.\n");
  printf(
"  -N             precompute the tetrahedra normals.\n");
  printf(
"  -n NAME        name of the medium. Default is \"%s\".\n",
    ATRSTM_ARGS_DEFAULT.name);
  printf(
"  -O CACHE       name of the cache file to use/fill. By default\n"
"                 no cache is used\n");
  printf(
"  -p THERMOPROPS path toward the thermodynamic properties.\n");
  printf(
"  -r REFRACT_ID  path toward the per wavelength refractive\n"
"                 indices.\n");
  printf(
"  -T THRESHOLD   optical thickness used as threshold during the octree\n"
"                 building. By default its value is %g.\n",
    ATRSTM_ARGS_DEFAULT.optical_thickness);
  printf(
"  -t NTHREADS    hint on the number of threads to use. By default use\n"
"                 as many threads as CPU cores.\n");
  printf(
"  -V <DEFINITION|X,Y,Z>\n"
"                 maximum definition of the acceleration grids\n"
"                 along the 3 axis. Its default value is [%u, %u, %u].\n",
    SPLIT3(ATRSTM_ARGS_DEFAULT.grid_max_definition));
  printf(
"  -v             make the program verbose.\n");
  printf(
"  -w WAVELENGTH  shortwave wavelength to use, in nanometer.\n"
"                 By default it is set to %g nm\n",
    ATRSTM_ARGS_DEFAULT.wlen_range[0]);
  printf("\n");
  printf(
"This is free software released under the GNU GPL license, version 3 or\n"
"later. You are free to change or redistribute it under certain\n"
"conditions <http://gnu.org.licenses/gpl.html>\n");
}

static res_T
parse_grid_definition(struct atrstm_args* args, const char* str)
{
  unsigned def[3];
  size_t len;
  res_T res = RES_OK;
  ASSERT(args && str);

  res = cstr_to_list_uint(str, ',', def, &len, 3);
  if(res == RES_OK && len == 2) res = RES_OK;
  if(res != RES_OK) {
    fprintf(stderr, "Invalid grid definition `%s'.\n", str);
    goto error;
  }

  if(len == 1) {
    if(!def[0]) {
      fprintf(stderr, "Invalid null grid definition %u.\n", def[0]);
      res = RES_BAD_ARG;
      goto error;
    }
    args->auto_grid_definition = 1;
    args->auto_grid_definition_hint = def[0];

  } else {
    if(!def[0] || !def[1] || !def[2]) {
      fprintf(stderr,
        "Invalid null grid definition [%u, %u, %u].\n", SPLIT3(def));
      res = RES_BAD_ARG;
      goto error;
    }
    args->auto_grid_definition = 0;
    args->grid_max_definition[0] = def[0];
    args->grid_max_definition[1] = def[1];
    args->grid_max_definition[2] = def[2];
  }

exit:
  return res;
error:
  goto exit;
}

static void
args_release(struct args* args)
{
  ASSERT(args);
  *args = ARGS_DEFAULT;
}

static res_T
args_init(struct args* args, int argc, char** argv)
{
  res_T res = RES_OK;
  int opt;
  ASSERT(args && argc && argv);

  *args = ARGS_DEFAULT;

  while((opt = getopt(argc, argv, "d:f:g:hm:Nn:O:p:T:t:r:vV:w:")) != -1) {
    switch(opt) {
      case 'd': args->dump_octree_filename = optarg; break;
      case 'f':
        res = cstr_to_double(optarg, &args->atrstm.fractal_dimension);
        if(res == RES_OK && args->atrstm.fractal_dimension <= 0)
          res = RES_BAD_ARG;
        break;
      case 'g':
        res = cstr_to_double(optarg, &args->atrstm.fractal_prefactor);
        if(res == RES_OK && args->atrstm.fractal_prefactor <= 0)
          res = RES_BAD_ARG;
        break;
      case 'h':
        print_help(argv[0]);
        args_release(args);
        args->quit = 1;
        goto exit;
      case 'm': args->atrstm.sth_filename = optarg; break;
      case 'N': args->atrstm.precompute_normals = 1; break;
      case 'n': args->atrstm.name = optarg; break;
      case 'O': args->atrstm.cache_filename = optarg; break;
      case 'p': args->atrstm.atrtp_filename = optarg; break;
      case 'r': args->atrstm.atrri_filename = optarg; break;
      case 'T':
        res = cstr_to_double(optarg, &args->atrstm.optical_thickness);
        if(res == RES_OK && args->atrstm.optical_thickness<0) res = RES_BAD_ARG;
        break;
      case 't':
        res = cstr_to_uint(optarg, &args->atrstm.nthreads);
        if(res == RES_OK && !args->atrstm.nthreads) res = RES_BAD_ARG;
        break;
      case 'V': res = parse_grid_definition(&args->atrstm, optarg); break;
      case 'v': args->atrstm.verbose = 1; break;
      case 'w':
        res = cstr_to_double(optarg, &args->atrstm.wlen_range[0]);
        if(res == RES_OK && args->atrstm.wlen_range[0] < 0) res = RES_BAD_ARG;
        args->atrstm.wlen_range[1] = args->atrstm.wlen_range[0];
        break;
      default: res = RES_BAD_ARG; break;
    }
    if(res != RES_OK) {
      if(optarg) {
        fprintf(stderr, "%s: invalid option argument '%s' -- '%c'\n",
          argv[0], optarg, opt);
      }
      goto error;
    }
  }

  /* Check parsed arguments */
  if(!args->atrstm.sth_filename) {
    fprintf(stderr,
      "Missing the path toward the volumetric mesh -- option '-m'\n");
    res = RES_BAD_ARG;
    goto error;
  }
  if(!args->atrstm.atrtp_filename) {
    fprintf(stderr,
      "Missing the path of the thermodynamic properties -- option '-p'\n");
    res = RES_BAD_ARG;
    goto error;
  }
  if(!args->atrstm.atrri_filename) {
    fprintf(stderr,
      "Missing the path of the refractive indices -- option '-r'\n");
    res = RES_BAD_ARG;
    goto error;
  }

exit:
  return res;
error:
  args_release(args);
  goto exit;
}

/*******************************************************************************
 * Main function
 ******************************************************************************/
int
main(int argc, char** argv)
{
  struct args args = ARGS_DEFAULT;
  struct atrstm* atrstm = NULL;
  FILE* fp_octree = NULL; 
  res_T res = RES_OK;
  int err = 0;

  res = args_init(&args, argc, argv);
  if(res != RES_OK) goto error;
  if(args.quit) goto exit;

  res = atrstm_create(NULL, &mem_default_allocator, &args.atrstm, &atrstm);
  if(res != RES_OK) goto error;

  if(args.dump_octree_filename) {
    const struct atrstm_dump_svx_octree_args dump_svx_octree_args = 
      ATRSTM_DUMP_SVX_OCTREE_ARGS_DEFAULT;

    fp_octree = fopen(args.dump_octree_filename, "w");
    if(!fp_octree) {
      fprintf(stderr, "Could not open `%s' -- %s.",
        args.dump_octree_filename, strerror(errno));
      res = RES_IO_ERR;
      goto error;
    }

    res = atrstm_dump_svx_octree(atrstm, &dump_svx_octree_args, fp_octree);
    if(res != RES_OK) goto error;
  }

exit:
  args_release(&args);
  if(atrstm) ATRSTM(ref_put(atrstm));
  if(fp_octree) fclose(fp_octree);
  if(MEM_ALLOCATED_SIZE(&mem_default_allocator) != 0) {
    fprintf(stderr, "Memory leaks: %lu bytes\n", 
      (unsigned long)MEM_ALLOCATED_SIZE(&mem_default_allocator));
    err = -1;
  }
  return err;
error:
  err = -1;
  goto exit;
}

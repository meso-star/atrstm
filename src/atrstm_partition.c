/* Copyright (C) 2022, 2023 |Méso|Star> (contact@meso-star.com)
 * Copyright (C) 2020, 2021 Centre National de la Recherche Scientifique
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "atrstm_log.h"
#include "atrstm_partition.h"

#include <rsys/condition.h>
#include <rsys/mem_allocator.h>
#include <rsys/mutex.h>

/******************************************************************************
 * Local functions
 ******************************************************************************/
res_T
pool_init
  (struct mem_allocator* mem_allocator,
   const size_t nparts,
   struct pool* pool)
{
  size_t ipart;
  res_T res = RES_OK;
  ASSERT(pool && nparts);

  memset(pool, 0, sizeof(*pool));

  list_init(&pool->parts_free);
  list_init(&pool->parts_full);
  pool->allocator = mem_allocator ? mem_allocator : &mem_default_allocator;

  pool->mutex = mutex_create();
  if(!pool->mutex) { res = RES_UNKNOWN_ERR; goto error; }
  pool->cond_new = cond_create();
  if(!pool->cond_new) { res = RES_UNKNOWN_ERR; goto error; }
  pool->cond_fetch = cond_create();
  if(!pool->cond_fetch) { res = RES_UNKNOWN_ERR; goto error; }

  FOR_EACH(ipart, 0, nparts) {
    struct part* part = MEM_ALLOC_ALIGNED(pool->allocator, sizeof(*part), 64);
    if(!part) {
      res = RES_MEM_ERR;
      goto error;
    }
    part_init(part);
    list_add(&pool->parts_free, &part->node);
  }

exit:
  return res;
error:
  pool_release(pool);
  goto exit;
}

void
pool_release(struct pool* pool)
{
  struct list_node* node;
  struct list_node* tmp_node;
  ASSERT(pool);

  if(pool->mutex) mutex_destroy(pool->mutex);
  if(pool->cond_new) cond_destroy(pool->cond_new);
  if(pool->cond_fetch) cond_destroy(pool->cond_fetch);

  if(pool->parts_free.next != NULL) { /* Is list initialised */
    LIST_FOR_EACH_SAFE(node, tmp_node, &pool->parts_free) {
      struct part* part = CONTAINER_OF(node, struct part, node);
      list_del(node);
      part_release(part);
      MEM_RM(pool->allocator, part);
    }
    ASSERT(is_list_empty(&pool->parts_free));
  }

  if(pool->parts_full.next != NULL) { /* Is list initialised */
    LIST_FOR_EACH_SAFE(node, tmp_node, &pool->parts_full) {
      struct part* part = CONTAINER_OF(node, struct part, node);
      list_del(node);
      part_release(part);
      MEM_RM(pool->allocator, part);
    }
    ASSERT(is_list_empty(&pool->parts_full));
  }

  memset(pool, 0, sizeof(*pool));
}

struct part*
pool_next_partition(struct pool* pool)
{
  struct list_node* node = NULL;
  struct part* part = NULL;
  ASSERT(pool);

  mutex_lock(pool->mutex);
  while(is_list_empty(&pool->parts_free) && !pool->error) {
    cond_wait(pool->cond_new, pool->mutex);
  }
  if(pool->error) {
    part = NULL;
    mutex_unlock(pool->mutex);
  } else {
    size_t ipart;
    node = list_head(&pool->parts_free);
    list_del(node);
    ipart = pool->next_part_id++;
    mutex_unlock(pool->mutex);

    part = CONTAINER_OF(node, struct part, node);
    part->id = ipart;
  }
  return part;
}

void
pool_free_partition(struct pool* pool, struct part* part)
{
  ASSERT(pool && part);

  mutex_lock(pool->mutex);
  list_move_tail(&part->node, &pool->parts_free);
  mutex_unlock(pool->mutex);

  cond_signal(pool->cond_new);
}

void
pool_commit_partition(struct pool* pool, struct part* part)
{
  struct list_node* node = NULL;
  ASSERT(pool && part);

  mutex_lock(pool->mutex);
  LIST_FOR_EACH_REVERSE(node, &pool->parts_full) {
    struct part* part2 = CONTAINER_OF(node, struct part, node);
    if(part2->id <= part->id) break;
  }
  list_add(node, &part->node);
  mutex_unlock(pool->mutex);

  cond_signal(pool->cond_fetch);
}

struct part*
pool_fetch_partition(struct pool* pool, const size_t part_id)
{
  struct part* found_part = NULL;
  struct list_node* node = NULL;
  ASSERT(pool);

  mutex_lock(pool->mutex);
  while(!pool->error) {
    /* Search for a partition that match the submitted id */
    LIST_FOR_EACH(node, &pool->parts_full) {
      struct part* part = CONTAINER_OF(node, struct part, node);

      if(part->id == part_id) {
        found_part = part;
        break;
      }

      /* Partitions are sorted in ascending order. Stop the linear search if the
       * current id is greater than the submitted one */
      if(part->id > part_id) break;
    }
    if(!found_part) {
      cond_wait(pool->cond_fetch, pool->mutex);
    } else {
      /*list_del(&found_part->node);*/
      break;
    }
  }
  mutex_unlock(pool->mutex);

  return found_part;
}

void
pool_invalidate(struct pool* pool)
{
  ASSERT(pool);
  mutex_lock(pool->mutex);
  pool->error = 1;
  mutex_unlock(pool->mutex);

  cond_broadcast(pool->cond_new);
  cond_broadcast(pool->cond_fetch);
}


/* Copyright (C) 2022, 2023 |Méso|Star> (contact@meso-star.com)
 * Copyright (C) 2020, 2021 Centre National de la Recherche Scientifique
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef ATRSTM_RDGFA_SIMD4_H
#define ATRSTM_RDGFA_SIMD4_H

#include <rsys/rsys.h>

#include <rsimd/math.h>
#include <rsimd/rsimd.h>

static FINLINE v4f_T /* In nanometer */
compute_gyration_radius_simd4
  (const v4f_T fractal_prefactor,
   const v4f_T fractal_dimension,
   const v4f_T soot_primary_particles_count,
   const v4f_T soot_primary_particles_diameter)
{
  const v4f_T kf = fractal_prefactor;
  const v4f_T Df = fractal_dimension;
  const v4f_T Np = soot_primary_particles_count;
  const v4f_T Dp = soot_primary_particles_diameter;
  const v4f_T tmp = v4f_pow(v4f_div(Np, kf), v4f_rcp(Df));
  const v4f_T Rg = v4f_mul(v4f_mul(v4f_set1(0.5f), Dp), tmp); /* [nm] */
  return Rg;
}

#endif /* ATRSTM_RDGFA_SIMD4_H */


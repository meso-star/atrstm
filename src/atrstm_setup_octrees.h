/* Copyright (C) 2022, 2023 |Méso|Star> (contact@meso-star.com)
 * Copyright (C) 2020, 2021 Centre National de la Recherche Scientifique
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef ATRSTM_SETUP_OCTREES_H
#define ATRSTM_SETUP_OCTREES_H

#include <rsys/rsys.h>

/* Forward declaration of external data type */
struct sth;

extern LOCAL_SYM res_T
setup_octrees
  (struct atrstm* atrstm);

extern LOCAL_SYM void
octrees_clean
  (struct atrstm* atrstm);

#endif /* ATRSTM_SETUP_OCTREES_H */


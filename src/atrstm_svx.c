/* Copyright (C) 2022, 2023 |Méso|Star> (contact@meso-star.com)
 * Copyright (C) 2020, 2021 Centre National de la Recherche Scientifique
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "atrstm.h"
#include "atrstm_c.h"
#include "atrstm_log.h"
#include "atrstm_svx.h"

#include <star/svx.h>

#include <rsys/cstr.h>

/*******************************************************************************
 * Helper functions
 ******************************************************************************/
static INLINE int
check_fetch_radcoefs_svx_args
  (const struct atrstm* atrstm,
   const struct atrstm_fetch_radcoefs_svx_args* args,
   const char* func_name)
{
  ASSERT(atrstm && func_name);

  if(!args
  || !(args->radcoefs_mask & ATRSTM_RADCOEFS_MASK_ALL)
  || !(args->components_mask & ATRSTM_CPNTS_MASK_ALL)
  || !(args->operations_mask & ATRSTM_SVX_OPS_MASK_ALL))
    return 0;

  if(atrstm->spectral_type != ATRSTM_SPECTRAL_SW) {
    log_err(atrstm, "%s: only shortwave is currently supported.\n",
      func_name);
    return 0;
  }

  return 1;
}

static INLINE int
check_fetch_radcoefs_svx_voxel_args
  (const struct atrstm* atrstm,
   const struct atrstm_fetch_radcoefs_svx_voxel_args* args,
   const char* func_name)
{
  ASSERT(atrstm && func_name);

  if(!args
  || SVX_VOXEL_NONE(&args->voxel)
  || !(args->radcoefs_mask & ATRSTM_RADCOEFS_MASK_ALL)
  || !(args->components_mask & ATRSTM_CPNTS_MASK_ALL)
  || !(args->operations_mask & ATRSTM_SVX_OPS_MASK_ALL))
    return 0;

  if(atrstm->spectral_type != ATRSTM_SPECTRAL_SW) {
    log_err(atrstm, "%s: only shortwave is currently supported.\n",
      func_name);
    return 0;
  }

  return 1;
}

static INLINE int
check_trace_ray_args
  (const struct atrstm* atrstm,
   const struct atrstm_trace_ray_args* args,
   const char* func_name)
{
  ASSERT(atrstm && func_name);

  if(!args) return 0;

  if(atrstm->spectral_type != ATRSTM_SPECTRAL_SW) {
    log_err(atrstm, "%s: only shortwave is currently supported.\n",
      func_name);
    return 0;
  }

  return 1;
}

static INLINE void
reset_radcoefs
  (double radcoefs[ATRSTM_RADCOEFS_COUNT__][ATRSTM_SVX_OPS_COUNT__])
{
  ASSERT(radcoefs);
  memset(radcoefs, 0,
    sizeof(double[ATRSTM_RADCOEFS_COUNT__][ATRSTM_SVX_OPS_COUNT__]));
}

/*******************************************************************************
 * Exported functions
 ******************************************************************************/
res_T
atrstm_fetch_radcoefs_svx
  (const struct atrstm* atrstm,
   const struct atrstm_fetch_radcoefs_svx_args* args,
   atrstm_radcoefs_svx_T radcoefs) /* In m^-1 */
{

  struct atrstm_fetch_radcoefs_svx_voxel_args voxel_args;
  struct svx_voxel voxel = SVX_VOXEL_NULL;
  res_T res = RES_OK;

  if(!atrstm
  || !check_fetch_radcoefs_svx_args(atrstm, args, FUNC_NAME)
  || !radcoefs) {
    res = RES_BAD_ARG;
    goto error;
  }

  /* Retrieve the voxel into which the queried position lies */
  SVX(tree_at(atrstm->octree, args->pos, NULL, NULL, &voxel));
  if(SVX_VOXEL_NONE(&voxel)) {
    log_warn(atrstm,
      "%s: the position {%g, %g, %g} does not belong to the Star-VoXel structure.\n"
      "medium.\n", FUNC_NAME, SPLIT3(args->pos));
    reset_radcoefs(radcoefs);
    goto exit;
  }

  /* Retrieve the radiative coefficients of the voxel */
  voxel_args = ATRSTM_FETCH_RADCOEFS_SVX_VOXEL_ARGS_DEFAULT;
  voxel_args.voxel = voxel;
  voxel_args.iband = args->iband;
  voxel_args.iquad = args->iquad;
  voxel_args.radcoefs_mask = args->radcoefs_mask;
  voxel_args.components_mask = args->components_mask;
  voxel_args.operations_mask = args->operations_mask;
  res = atrstm_fetch_radcoefs_svx_voxel(atrstm, &voxel_args, radcoefs);
  if(res != RES_OK) goto error;

exit:
  return res;
error:
  goto exit;
}

res_T
atrstm_fetch_radcoefs_svx_voxel
  (const struct atrstm* atrstm,
   const struct atrstm_fetch_radcoefs_svx_voxel_args* args,
   atrstm_radcoefs_svx_T radcoefs) /* In m^-1 */
{
  int cpnt, radcoef, op;
  res_T res = RES_OK;

  if(!atrstm
  || !check_fetch_radcoefs_svx_voxel_args(atrstm, args, FUNC_NAME)
  || !radcoefs) {
    res = RES_BAD_ARG;
    goto error;
  }

  reset_radcoefs(radcoefs);

  FOR_EACH(cpnt, 0, ATRSTM_CPNTS_COUNT__) {
    if((args->components_mask & BIT(cpnt)) == 0) continue;

    FOR_EACH(radcoef, 0, ATRSTM_RADCOEFS_COUNT__) {
      if((args->radcoefs_mask & BIT(radcoef)) == 0) continue;

      FOR_EACH(op, 0, ATRSTM_SVX_OPS_COUNT__) {
        double k;
        if((args->operations_mask & BIT(op)) == 0) continue;

        /* Add component contribution to the current radiative coef */
        k = vox_get(args->voxel.data, cpnt, radcoef, op);
        radcoefs[radcoef][op] += k;
      }
    }
  }

exit:
  return res;
error:
  goto exit;
}

res_T
atrstm_trace_ray
  (const struct atrstm* atrstm,
   const struct atrstm_trace_ray_args* args,
   struct svx_hit* hit)
{
  res_T res = RES_OK;
  if(!atrstm || !check_trace_ray_args(atrstm, args, FUNC_NAME) || !hit) {
    res = RES_BAD_ARG;
    goto error;
  }

  /* Reset the hit */
  *hit = SVX_HIT_NULL;

  res = svx_tree_trace_ray(atrstm->octree, args->ray_org, args->ray_dir,
    args->ray_range, args->challenge, args->filter, args->context, hit);
  if(res != RES_OK) {
    log_err(atrstm, 
      "%s: error tracing the ray: origin = {%g, %g, %g}; "
      "direction = {%g, %g, %g}; range = {%g, %g} -- %s.\n",
      FUNC_NAME, SPLIT3(args->ray_org), SPLIT3(args->ray_dir),
      SPLIT2(args->ray_range), res_to_cstr(res));
    goto error;
  }

exit:
  return res;
error:
  goto exit;
}

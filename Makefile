# Copyright (C) 2022, 2023 |Méso|Star> (contact@meso-star.com)
# Copyright (C) 2020, 2021 Centre National de la Recherche Scientifique
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

.POSIX:
.SUFFIXES: # Clean up default inference rules

include config.mk

LIBNAME_STATIC = libatrstm.a
LIBNAME_SHARED = libatrstm.so
LIBNAME = $(LIBNAME_$(LIB_TYPE))

################################################################################
# Library building
################################################################################
SRC_REGULAR =\
 src/atrstm.c\
 src/atrstm_cache.c\
 src/atrstm_dump_svx_octree.c\
 src/atrstm_log.c\
 src/atrstm_partition.c\
 src/atrstm_radcoefs.c\
 src/atrstm_rdgfa.c\
 src/atrstm_setup_octrees.c\
 src/atrstm_setup_uvm.c\
 src/atrstm_svx.c
SRC_SIMD =\
 $(SRC_REGULAR)\
 src/atrstm_radcoefs_simd4.c
SRC = $(SRC_$(INSTRUCTION_SET))
OBJ = $(SRC:.c=.o)
DEP = $(SRC:.c=.d)

build_library: .config $(DEP)
	@$(MAKE) -fMakefile $$(for i in $(DEP); do echo -f $${i}; done) \
	$$(if $(RSIMD_EXISTS); then \
	     echo "INSTRUCTION_SET=SIMD"; \
	   else \
	     echo "INSTRUCTION_SET=REGULAR"; \
	   fi) \
	$$(if [ -n "$(LIBNAME)" ]; then \
	     echo "$(LIBNAME)"; \
	   else \
	     echo "$(LIBNAME_SHARED)"; \
	   fi)

$(DEP) $(OBJ): config.mk

$(LIBNAME_SHARED): $(OBJ)
	$(CC) $(CFLAGS_SO) $(RSYS_CFLAGS) -o $@ $(OBJ) $(LDFLAGS_SO) $(DPDC_LIBS)

$(LIBNAME_STATIC): libatrstm.o
	$(AR) -rc $@ $?
	$(RANLIB) $@

libatrstm.o: $(OBJ)
	$(LD) -r $(OBJ) -o $@
	$(OBJCOPY) $(OCPFLAGS) $@

.config: config.mk
	@if ! $(PKG_CONFIG) --atleast-version $(ATRRI_VERSION) atrri; then \
	  echo "atrri $(ATRRI_VERSION) not found" >&2; exit 1; fi
	@if ! $(PKG_CONFIG) --atleast-version $(ATRTP_VERSION) atrtp; then \
	  echo "atrtp $(ATRTP_VERSION) not found" >&2; exit 1; fi
	@if $(RSIMD_EXISTS); then \
	   if ! $(PKG_CONFIG) --atleast-version $(RSIMD_VERSION) rsimd; then \
	    echo "rsimd $(RSIMD_VERSION) not found" >&2; exit 1; fi; fi
	@if ! $(PKG_CONFIG) --atleast-version $(RSYS_VERSION) rsys; then \
	  echo "rsys $(RSYS_VERSION) not found" >&2; exit 1; fi
	@if ! $(PKG_CONFIG) --atleast-version $(SMSH_VERSION) smsh; then \
	  echo "smsh $(SMSH_VERSION) not found" >&2; exit 1; fi
	@if ! $(PKG_CONFIG) --atleast-version $(SUVM_VERSION) suvm; then \
	  echo "suvm $(SUVM_VERSION) not found" >&2; exit 1; fi
	@if ! $(PKG_CONFIG) --atleast-version $(SVX_VERSION) svx; then \
	  echo "svx $(SVX_VERSION) not found" >&2; exit 1; fi
	@echo "config done" > $@

.SUFFIXES: .c .d .o
.c.d:
	@$(CC) $(CFLAGS_SO) $(DPDC_CFLAGS) -MM -MT "$(@:.d=.o) $@" $< -MF $@

.c.o:
	$(CC) $(CFLAGS_SO) $(DPDC_CFLAGS) -DATRSTM_SHARED_BUILD -c $< -o $@

################################################################################
# Installation
################################################################################
pkg:
	sed -e 's#@PREFIX@#$(PREFIX)#g'\
	    -e 's#@VERSION@#$(VERSION)#g'\
	    -e 's#@ATRRI_VERSION@#$(ATRRI_VERSION)#g'\
	    -e 's#@ATRTP_VERSION@#$(ATRTP_VERSION)#g'\
	    -e 's#@RSYS_VERSION@#$(RSYS_VERSION)#g'\
	    -e 's#@SMSH_VERSION@#$(SMSH_VERSION)#g'\
	    -e 's#@SUVM_VERSION@#$(SUVM_VERSION)#g'\
	    -e 's#@SVX_VERSION@#$(SVX_VERSION)#g'\
	    atrstm.pc.in | \
	if $(RSIMD_EXISTS); then \
	  sed 's#@RSIMD@#, rsimd >= $(RSIMD_VERSION)#g'; \
	else \
	  sed 's#@RSIMD@##g'; \
	fi > atrstm.pc

atrstm-local.pc: atrstm.pc.in
	sed -e '1d'\
	    -e 's#^includedir=.*#includedir=./src/#'\
	    -e 's#^libdir=.*#libdir=./#'\
	    -e 's#@VERSION@#$(VERSION)#g'\
	    -e 's#@ATRRI_VERSION@#$(ATRRI_VERSION)#g'\
	    -e 's#@ATRTP_VERSION@#$(ATRTP_VERSION)#g'\
	    -e 's#@RSYS_VERSION@#$(RSYS_VERSION)#g'\
	    -e 's#@SMSH_VERSION@#$(SMSH_VERSION)#g'\
	    -e 's#@SUVM_VERSION@#$(SUVM_VERSION)#g'\
	    -e 's#@SVX_VERSION@#$(SVX_VERSION)#g'\
	    atrstm.pc.in | \
	if $(RSIMD_EXISTS); then \
	  sed 's#@RSIMD@#, rsimd >= $(RSIMD_VERSION)#g'; \
	else \
	  sed 's#@RSIMD@##g'; \
	fi > $@

install: build_library pkg
	@$(SHELL) make.sh install "$(DESTDIR)$(PREFIX)/lib" $(LIBNAME)
	@$(SHELL) make.sh install "$(DESTDIR)$(PREFIX)/lib/pkgconfig" atrstm.pc
	@$(SHELL) make.sh install "$(DESTDIR)$(PREFIX)/include/astoria" src/atrstm.h
	@$(SHELL) make.sh install "$(DESTDIR)$(PREFIX)/share/doc/atrstm" COPYING README.md

uninstall:
	rm -f "$(DESTDIR)$(PREFIX)/lib/$(LIBNAME)"
	rm -f "$(DESTDIR)$(PREFIX)/lib/pkgconfig/atrstm.pc"
	rm -f "$(DESTDIR)$(PREFIX)/share/doc/atrstm/COPYING"
	rm -f "$(DESTDIR)$(PREFIX)/share/doc/atrstm/README.md"
	rm -f "$(DESTDIR)$(PREFIX)/include/astoria/atrstm.h"

################################################################################
# Miscellaneous targets
################################################################################
all: build_library build_tests

clean: clean_test
	rm -f $(SRC_COMMON:.c=.o) $(SRC_SIMD:.c=.o) $(TEST_OBJ) $(LIBNAME)
	rm -f .config .test libatrstm.o atrstm.pc atrstm-local.pc

distclean: clean
	rm -f $(SRC_COMMON:.c=.d) $(SRC_SIMD:.c=.d) $(TEST_DEP)

lint:
	shellcheck -o all make.sh

################################################################################
# Tests
################################################################################
TEST_SRC =\
 src/test_atrstm.c\
 src/test_atrstm_radcoefs.c\
 src/test_atrstm_radcoefs_simd.c
TEST_OBJ = $(TEST_SRC:.c=.o)
TEST_DEP = $(TEST_SRC:.c=.d)

PKG_CONFIG_LOCAL = PKG_CONFIG_PATH="./:$${PKG_CONFIG_PATH}" $(PKG_CONFIG)
ATRSTM_CFLAGS = $$($(PKG_CONFIG_LOCAL) $(PCFLAGS) --cflags atrstm-local.pc)
ATRSTM_LIBS = $$($(PKG_CONFIG_LOCAL) $(PCFLAGS) --libs atrstm-local.pc)

test: build_tests
	@$(SHELL) make.sh run_test \
	src/test_atrstm_radcoefs.c \
	$$($(RSIMD_EXISTS) && echo src/test_atrstm_radcoefs_simd.c)

build_tests: build_library $(TEST_DEP) .test
	@$(MAKE) -fMakefile -f.test $$(for i in $(TEST_DEP); do echo -f"$${i}"; done) test_bin

.test: Makefile make.sh
	@$(SHELL) make.sh config_test \
	src/test_atrstm.c \
	src/test_atrstm_radcoefs.c \
	$$($(RSIMD_EXISTS) && echo src/test_atrstm_radcoefs_simd.c)	\
	> $@

clean_test:
	$(SHELL) make.sh clean_test $(TEST_SRC)

$(TEST_DEP): config.mk atrstm-local.pc
	@$(CC) $(CFLAGS_EXE) $(RSYS_CFLAGS) $(ATRSTM_CFLAGS) \
	-MM -MT "$(@:.d=.o) $@" $(@:.d=.c) -MF $@

src/test_atrstm.o src/test_atrstm_radcoefs.o: config.mk atrstm-local.pc
	$(CC) $(CFLAGS_EXE) $(ATRSTM_CFLAGS) $(RSYS_CFLAGS) -c $(@:.o=.c) -o $@

src/test_atrstm_radcoefs_simd.o: config.mk atrstm-local.pc
	$(CC) $(CFLAGS_EXE) $(ATRSTM_CFLAGS) $(RSYS_CFLAGS) $(RSIMD_CFLAGS) -c $(@:.o=.c) -o $@

test_atrstm test_atrstm_radcoefs: config.mk atrstm-local.pc $(LIBNAME)
	$(CC) $(CFLAGS_EXE) -o $@ src/$@.o $(LDFLAGS_EXE) $(ATRSTM_LIBS) $(RSYS_LIBS) -lm

test_atrstm_radcoefs_simd: config.mk atrstm-local.pc $(LIBNAME)
	$(CC) $(CFLAGS_EXE) -o $@ src/$@.o $(LDFLAGS_EXE) $(ATRSTM_LIBS) $(RSYS_LIBS) $(RSIMD_LIBS)

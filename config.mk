VERSION = 0.1.1
PREFIX = /usr/local

LIB_TYPE = SHARED
#LIB_TYPE = STATIC

BUILD_TYPE = RELEASE
#BUILD_TYPE = DEBUG

################################################################################
# Tools
################################################################################
AR = ar
CC = cc
LD = ld
OBJCOPY = objcopy
PKG_CONFIG = pkg-config
RANLIB = ranlib

################################################################################
# Dependencies
################################################################################
PCFLAGS_SHARED =
PCFLAGS_STATIC = --static
PCFLAGS = $(PCFLAGS_$(LIB_TYPE))

ATRRI_VERSION = 0.1
ATRRI_CFLAGS = $$($(PKG_CONFIG) $(PCFLAGS) --cflags atrri)
ATRRI_LIBS = $$($(PKG_CONFIG) $(PCFLAGS) --libs atrri)

ATRTP_VERSION = 0.1
ATRTP_CFLAGS = $$($(PKG_CONFIG) $(PCFLAGS) --cflags atrtp)
ATRTP_LIBS = $$($(PKG_CONFIG) $(PCFLAGS) --libs atrtp)

RSIMD_VERSION = 0.5
RSIMD_EXISTS = $(PKG_CONFIG) --exists rsimd
RSIMD_CFLAGS = $$($(RSIMD_EXISTS) && $(PKG_CONFIG) $(PCFLAGS) --cflags rsimd)
RSIMD_LIBS = $$($(RSIMD_EXISTS) && $(PKG_CONFIG) $(PCFLAGS) --libs rsimd)

RSYS_VERSION = 0.14
RSYS_CFLAGS = $$($(PKG_CONFIG) $(PCFLAGS) --cflags rsys)
RSYS_LIBS = $$($(PKG_CONFIG) $(PCFLAGS) --libs rsys)

SMSH_VERSION = 0.1
SMSH_CFLAGS = $$($(PKG_CONFIG) $(PCFLAGS) --cflags smsh)
SMSH_LIBS = $$($(PKG_CONFIG) $(PCFLAGS) --libs smsh)

SUVM_VERSION = 0.3
SUVM_CFLAGS = $$($(PKG_CONFIG) $(PCFLAGS) --cflags suvm)
SUVM_LIBS = $$($(PKG_CONFIG) $(PCFLAGS) --libs suvm)

SVX_VERSION = 0.3
SVX_CFLAGS = $$($(PKG_CONFIG) $(PCFLAGS) --cflags svx)
SVX_LIBS = $$($(PKG_CONFIG) $(PCFLAGS) --libs svx)

DPDC_CFLAGS =\
 $(ATRRI_CFLAGS)\
 $(ATRTP_CFLAGS)\
 $(RSIMD_CFLAGS)\
 $(RSYS_CFLAGS)\
 $(SMSH_CFLAGS)\
 $(SUVM_CFLAGS)\
 $(SVX_CFLAGS)\
 -fopenmp
DPDC_LIBS=\
 $(ATRRI_LIBS)\
 $(ATRTP_LIBS)\
 $(RSIMD_LIBS)\
 $(RSYS_LIBS)\
 $(SMSH_LIBS)\
 $(SUVM_LIBS)\
 $(SVX_LIBS)\
 -fopenmp\
 -lm

################################################################################
# Compilation options
################################################################################
WFLAGS =\
 -Wall\
 -Wcast-align\
 -Wconversion\
 -Wextra\
 -Wmissing-declarations\
 -Wmissing-prototypes\
 -Wshadow

CFLAGS_HARDENED =\
 -D_FORTIFY_SOURCES=2\
 -fcf-protection=full\
 -fstack-clash-protection\
 -fstack-protector-strong

CFLAGS_COMMON =\
 -std=c89\
 -pedantic\
 -fvisibility=hidden\
 -fstrict-aliasing\
 $$($(RSIMD_EXISTS) && echo "-DATRSTM_USE_SIMD")\
 $(CFLAGS_HARDENED)\
 $(WFLAGS)

CFLAGS_DEBUG = -g $(CFLAGS_COMMON)
CFLAGS_RELEASE = -O2 -DNDEBUG $(CFLAGS_COMMON)
CFLAGS = $(CFLAGS_$(BUILD_TYPE))

CFLAGS_SO = $(CFLAGS) -fPIC
CFLAGS_EXE = $(CFLAGS) -fPIE

################################################################################
# Linker options
################################################################################
LDFLAGS_HARDENED = -Wl,-z,relro,-z,now
LDFLAGS_DEBUG = $(LDFLAGS_HARDENED)
LDFLAGS_RELEASE = -s $(LDFLAGS_HARDENED)
LDFLAGS = $(LDFLAGS_$(BUILD_TYPE))

LDFLAGS_SO = $(LDFLAGS) -shared -Wl,--no-undefined
LDFLAGS_EXE = $(LDFLAGS) -pie

OCPFLAGS_DEBUG = --localize-hidden
OCPFLAGS_RELEASE = --localize-hidden --strip-unneeded
OCPFLAGS = $(OCPFLAGS_$(BUILD_TYPE))

# AsToRia: Semi-Transparent Medium

This C library loads and manages the geometric data and physical
properties of a gas mixture.

## Requirements

- C compiler with OpenMP support
- POSIX make
- pkg-config
- [Astoria: Thermodynamic Properties](https://gitlab.com/meso-star/atrtp)
- [Astoria: Refractive Index](https://gitlab.com/meso-star/atrri)
- [RSIMD](https://gitlab.com/vaplv/rsimd) (optional)
- [RSys](https://gitlab.com/vaplv/rsys)
- [Star Mesh](https://gitlab.com/meso-star/star-mesh)
- [Star Unstructured Volumetric Mesh](https://gitlab.com/meso-star/star-uvm)
- [Star VoXel](https://gitlab.com/meso-star/star-vx)

## Installation

Edit config.mk as needed, then run:

    make clean install

## Release notes

### Version 0.1.1

- Fix compilation with SIMD support: when the rsimd library was found,
  the library depended on it, but without activating the SIMD
  instruction set.

### Version 0.1

- Replace CMake by Makefile as build system.
- Update compiler and linker flags to increase the security and
  robustness of generated binaries.
- Provide a pkg-config file to link the library as an external
  dependency.

### Version 0.0.1

- Relies on the Star-Mesh library rather than the obsolete
  Star-Tetrahedra library.
- Fix a possible memory leak.
- Fix a wrong assertion.

## Copyright

Copyright (C) 2022, 2023 |Méso|Star> (contact@meso-star.com)  
Copyright (C) 2020, 2021 Centre National de la Recherche Scientifique (CNRS)

## License

AtrSTM is free software released under the GPL v3+ license: GNU GPL
version 3 or later. You are welcome to redistribute it under certain
conditions; refer to the COPYING file for details.
